#! /usr/bin/env make -rf
VERSION ?= 5.3.6
TMPDIR := $(shell mktemp -d)

%/src/lua-${VERSION} : ${TMPDIR}/lua-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/${package_name}-% :
	curl -fL -o "$@" "https://www.lua.org/ftp/$(@F)"
