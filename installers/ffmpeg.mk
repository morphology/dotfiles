#! /usr/bin/make -rf
package_name := ffmpeg
include _defaults.mk
VERSION != python3 -m helper.release_version latest 5405

@install : /usr/src/${package_name}
ifneq (,${dependencies})
	${SUDO} apt-get install ${dependencies}
endif
	sh "${package_name}.sh" "$<" /usr/local/stow/${package_name}-${VERSION}
	sh "${package_name}-stow.sh" "${package_name}-${VERSION}"

# These do not share a version

%/src/ffmpeg :
	git clone --depth 1 -b n${VERSION} "https://git.ffmpeg.org/$(@F).git" "$@"

%/src/vid.stab :
	git clone --depth 1 -b v${VERSION} "https://github.com/georgmartius/$(@F).git" "$@"
