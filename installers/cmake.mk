#! /usr/bin/make -rf
include _defaults.mk

%/src/cmake-${VERSION} : ${DOWNLOAD_DIR}/cmake-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/cmake-%.tar.gz : ${DOWNLOAD_DIR}/cmake-%-SHA-256.txt
	wget --no-clobber -P "$(@D)" \
		"https://github.com/Kitware/CMake/releases/download/v${VERSION}/$(@F)"
	cd ${DOWNLOAD_DIR} && sha256sum --ignore-missing -c $<

${DOWNLOAD_DIR}/cmake-%-SHA-256.txt : /usr/share/keyrings/kitware-archive-keyring.gpg
	wget --no-clobber -P "$(@D)" \
		"https://github.com/Kitware/CMake/releases/download/v${VERSION}/$(@F)" \
		"https://github.com/Kitware/CMake/releases/download/v${VERSION}/$(@F).asc"
	gpgv --keyring $< "$@.asc" "$@"

%/keyrings/kitware-archive-keyring.gpg :
	curl "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xcba23971357c2e6590d9efd3ec8fef3a7bfb4eda" | \
		${SUDO} gpg --no-default-keyring --keyring "$@" --import

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
