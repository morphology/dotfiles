#! /usr/bin/env make -rf
VERSION ?= 21.2

%/src/${package_name}:
	git clone --depth 1 -b cjdns-v${VERSION} "https://github.com/cjdelisle/$(@F).git" "$@"

%/cjdroute.conf :
	install -d "$(@D)"
	cjdroute --genconf >> "$@"
	chmod 0600 "$@"

%.service :
	install -D src/${package_name}/contrib/systemd/$(@F) "$@"
