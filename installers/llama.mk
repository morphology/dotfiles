#! /usr/bin/make -rf
arch := x86_64
export HF_HUB_ENABLE_HF_TRANSFER = 1

### Example models:
# bakllava/ggml-model-q4_k.gguf bakllava/mmproj-model-f16.gguf
# llava-v1.5-7b-Q4_K.gguf, lava-v1.5-7b-mmproj-Q4_0.gguf
# mistral-7b-instruct-v0.1.Q4_K_M.gguf
# orca-2-13b-16k-q4_k_m.gguf
# phi-2.Q5_K_M.gguf
# replit-code-v1_5-3b.f16.gguf
# wizardcoder-python-13b-v1.0.Q5_K_M.gguf


models/bakllava/% :
	huggingface-cli download --local-dir=$(@D) \
		mys/ggml_bakllava-1 \
		"$(@F)"

models/llava-v1.5-7b-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		jartine/llava-v1.5-7B-GGUF \
		"$(@F)"

models/mistral-7b-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		TheBloke/Mistral-7B-Instruct-v0.1-GGUF \
		"$(@F)"

models/mixtral-8x7b-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		TheBloke/Mixtral-8x7B-Instruct-v0.1-GGUF \
		"$(@F)"

models/orca-2-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		NurtureAI/Orca-2-13B-16k-GGUF \
		"$(@F)"
		

models/phi-2%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		TheBloke/phi-2-GGUF \
		"$(@F)"

models/replit-code-v1_5-3b%.gguf :
	# https://llama-cpp-python.readthedocs.io/en/latest/server/#code-completion
	huggingface-cli download --local-dir=$(@D) \
		abetlen/replit-code-v1_5-3b-GGUF
		"$(@F)"

models/wizardcoder-python-7b-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		TheBloke/WizardCoder-Python-7B-V1.0-GGUF \
		"$(@F)"

models/wizardcoder-python-13b-%.gguf :
	huggingface-cli download --local-dir=$(@D) \
		TheBloke/WizardCoder-Python-13B-V1.0-GGUF \
		"$(@F)"


%/src/llama.cpp :
	git clone --depth=1 "https://github.com/ggerganov/$(@F)" "$@"

${DOWNLOAD_DIR}/cuda-keyring_%.deb :
	wget --no-clobber -P "$(@D)" \
		"https://developer.download.nvidia.com/compute/cuda/repos/wsl-ubuntu/${arch}/$(@F)"

