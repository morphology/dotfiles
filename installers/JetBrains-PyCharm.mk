#! /usr/bin/make -rf
# or pycharm for Professional
package_name := pycharm-community
include _defaults.mk
VERSION != python3 -m helper.release_version latest 227263
install_prefix = /opt/${package_name}-${VERSION}

@install : ${install_prefix}

%/pycharm-community-${VERSION} : ${DOWNLOAD_DIR}/pycharm-community-${VERSION}.tar.gz
	tar -C "$(@D)" -xzf "$<"
	chmod g+rwX-s -R "$@"
	sh JetBrains-PyCharm.sh '$@'

%/pycharm-${VERSION} : ${DOWNLOAD_DIR}/pycharm-professional-${VERSION}.tar.gz
	tar -C "$(@D)" -xzf "$<"
	chmod g+rwX-s -R "$@"
	sh JetBrains-PyCharm.sh '$@'

${DOWNLOAD_DIR}/pycharm-% :
	# gpg signature not offered
	wget --https-only -P "$(@D)" \
		"https://download.jetbrains.com/python/$(@F)" \
		"https://download.jetbrains.com/python/$(@F).sha256"
	cd "$(@D)" && sha256sum -c "$(@F).sha256"

# vim: noexpandtab ff=unix
