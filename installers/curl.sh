#! /bin/sh
package_name=curl
anitya_project_id=381
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/curl
cd ~/build/curl
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=/etc \
    --enable-versioned-symbols \
    --with-ca-path=/etc/ssl/certs/ \
    --with-openssl="${OPENSSL_ROOT_DIR?}"
  
  make all -j -l $(nproc)
  make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
