#! /usr/bin/make -rf
include _defaults.mk
arch ?= linux-amd64

%/build/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/nebula-${arch}.tar.gz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"

${DOWNLOAD_DIR}/nebula-% :
	wget -P "$(@D)" \
		"https://github.com/slackhq/${package_name}/releases/download/v${VERSION}/$(@F)"

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
