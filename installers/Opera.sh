#! /bin/sh
package_name=Opera
anitya_project_id=7242
set -e

. _hooks/setup.sh
make -rf "${package_name}.mk" install
