#! /bin/sh
set -eu
install_prefix="$1"
command shift

case "$package_name" in
	pycharm-community)
		install_prefix="/opt/pycharm-community-$VERSION"
		StartupWMClass=jetbrains-pycharm-ce
		;;
	pycharm-professional)
		install_prefix="/opt/pycharm-$VERSION"
		StartupWMClass=jetbrains-pycharm
		;;
esac
[ -f "${install_prefix}/bin/pycharm.sh" ] || exit 10
<< EOF sh _hooks/write_desktop_file.sh \
	--copy-name-to-generic-name \
	--dir=/usr/local/share/applications \
	--set-icon="${install_prefix}/bin/pycharm.svg" \
	--vendor=jetbrains
[Desktop Entry]
Type=Application
Version=1.0
Name=${package_name} ${VERSION}
StartupWMClass=${StartupWMClass}
Exec="${install_prefix}/bin/pycharm.sh" %u
Terminal=false
Categories=Development;IDE;Java;
EOF

if [ -d "$install_prefix/debug-eggs" ]; then
	install -C -D "${install_prefix}/debug-eggs"/* ~/python/
fi
