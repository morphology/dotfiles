#! /usr/bin/make -rf
VERSION ?= 2.0.2
install_prefix ?= ${HOME}

@install :
	go install github.com/sigstore/cosign/v2/cmd/cosign@v${VERSION}
	install -Ds ~/go/bin/cosign ${install_prefix}/bin/cosign
