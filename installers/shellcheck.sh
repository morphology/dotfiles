#! /bin/sh
. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

cabal update
cabal install ShellCheck

install -s ~/.cabal/bin/${package_name} ${install_prefix}/bin/
