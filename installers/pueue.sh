#! /bin/sh
set -eu

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

cargo install --locked ${package_name}

cd "$installer_dir"
  stow -S --no-folding "$package_name"
  systemctl daemon-reload --user
  install -D -s "${HOME}/.cargo/bin/pueue" "${HOME}/bin"
  install -D -s "${HOME}/.cargo/bin/pueued" "${HOME}/sbin"
  make -rf "${package_name}.mk" \
    "${HOME}/.local/etc/bash_completion.d/${package_name}"
cd -

echo "systemctl --user enable --now pueue.service"
