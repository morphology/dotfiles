#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/libptytty-% :
	wget -P "$(@D)" \
		"http://dist.schmorp.de/libptytty/$(@F)" \
		"http://dist.schmorp.de/libptytty/$(@F).sig"
	-gpgv "$@.sig"
