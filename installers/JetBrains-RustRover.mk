#! /usr/bin/make -rf
include _defaults.mk

VERSION ?= 233.13135.116

%/RustRover-${VERSION} : ${DOWNLOAD_DIR}/RustRover-${VERSION}.tar.gz
	${SUDO} install -g staff -m 0775 -d "$(@D)"
	${SUDO} chmod g+s "$(@D)"
	tar -C "$(@D)" -xzf "$<"

${DOWNLOAD_DIR}/RustRover-% :
	wget -P "$(@D)" --no-clobber \
		"https://download.jetbrains.com/rustrover/$(@F)" \
		"https://download.jetbrains.com/rustrover/$(@F).sha256"
	cd "$(@D)" ; sha256sum -c "$(@F).sha256"
