#! /bin/sh
package_name=nv-codec-headers
anitya_project_id=223796
set -e

. _hooks/setup.sh
source_dir=${HOME}/src/${package_name?}
make -rf nvidia.mk "$source_dir"

cd "$source_dir"
	# installs to /usr/local
	make install PREFIX="$install_prefix"
cd -

 . ${hooks_dir}/post_root_install.sh
