#! /usr/bin/make -rf
package_name := exa
VERSION != python3 -m helper.release_version latest 16575
include _defaults.mk

repo_directory := $(lastword $(wildcard ${HOME}/.cargo/registry/src/*))
module_directory = ${repo_directory}/${package_name}-${VERSION}
$(info Expecting exa module installed in ${module_directory})

@install : /usr/local/bin/${package_name} ${BASH_COMPLETION_USER_DIR}/completions/${package_name} /usr/local/share/man/man1/${package_name}.1 /usr/local/share/man/man5/exa_colors.5

%/completions/${package_name} : | ${module_directory}
	install -CD "$|/completions/completions.bash" "$@"
	bash -n "$@"

%/man/man1/exa.1 %/man/man5/exa_colors.5 : | ${module_directory}
	${SUDO} install -m 0755 -d "$(@D)"
	${SUDO} pandoc -s -t man "$|/man/$(@F).md" -o "$@"

%/bin/exa : @bin
	install -Ds ~/.cargo/bin/exa "$@"

@bin ${module_directory} :
ifneq (,${dependencies})
	${SUDO} apt-get update
	${SUDO} apt-get install ${dependencies}
endif
	cargo install exa
