#! /bin/sh
package_name=binutils-gdb
anitya_project_id=11798
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh
build_dir=~/build/${package_name}
mkdir -p "$build_dir"
cd "$build_dir"
  CC=gcc "${source_dir}/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --sysconfdir=/etc \
    --disable-nls --enable-host-shared \
    --with-python=/usr/bin/python3 \
    --with-system-readline --with-system-zlib --with-zstd
  make
  $SUDO make install
cd -

<< EOF cat > "${install_prefix}/.stow-local-ignore"
share/info/dir
EOF
