#! /usr/bin/make -rf
package_name ?= vscode
include _defaults.mk

@upgrade : /etc/apt/sources.list.d/${package_name}.list
	apt-get install apt-transport-https
	apt-get update
ifneq (,${UBUNTU_DEPS})
	apt-get install --no-install-suggests --no-install-recommends ${UBUNTU_DEPS}
endif

%/${package_name}.list : /etc/apt/trusted.gpg.d/packages.microsoft.gpg
	$(file > $@,deb [arch=amd64,arm64,armhf signed-by=$<] https://packages.microsoft.com/repos/code stable main)

%/packages.microsoft.gpg :
	curl -fL "https://packages.microsoft.com/keys/microsoft.asc" | \
		gpg --no-default-keyring --keyring="$(abspath $@)" --import
