#! /bin/sh
set -eu

<< EOF cat > ~/etc/bw-unlock.shrc
if BW_SESSION=\$(bw unlock --raw); then
	export BW_SESSION
	echo 'You may want to "bw login" if not already, and then "bw sync".'
else
	unset BW_SESSION
fi >&2
bw update
EOF
