#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name} :
	git clone --depth=1 -b "${VERSION}" \
		"https://github.com/tomscii/${package_name}.git" "$@"
