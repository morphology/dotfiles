#! /usr/bin/env make -rf
VERSION ?= 0.10
TMPDIR := $(shell mktemp -d)

%/src/minisign :
	git clone --depth=1 -b "${VERSION}" "https://github.com/jedisct1/${package_name}" "$@"

${HOME}/.minisign/minisign.key :
	install -d "$(@D)"
	${install_prefix}/bin/minisign -G
	mv -i minisign.pub ${HOME}/minisign.pub 
