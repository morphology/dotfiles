#! /bin/sh
package_name=dcmtk
anitya_project_id=18627
set -e


. _hooks/setup.sh
source_dir="/usr/local/src/${package_name}"
fmjpeg2koj_source_dir="/usr/local/src/fmjpeg2koj"
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$fmjpeg2koj_source_dir"

mkdir -p ~/build/dcmtk
cd ~/build/dcmtk
  # OPENSSL_ROOT_DIR for any version up to 3.0.0
  cmake "$source_dir" \
    -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DBUILD_SHARED_LIBS="ON" \
    -DDCMTK_ENABLE_CXX11="ON" \
    -DDCMTK_ENABLE_STL="ON"
  make -j -l $(nproc)
  make install
cd -


case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac

# JPEG2000 plugin
mkdir -p ~/build/fmjpeg2koj
cd ~/build/fmjpeg2koj
  cmake "$fmjpeg2koj_source_dir" \
    -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DBUILD_SHARED_LIBS="ON" \
    -DBUILD_STATIC_LIBS="OFF" \
    -DDCMTK_ROOT="$install_prefix" \
    -Dfmjpeg2k_ROOT="$fmjpeg2koj_source_dir" \
    -DOpenJPEG_ROOT="/usr"
  make -j -l $(nproc)
  make install
cd -

. ${hooks_dir}/install_ready.sh
install -d "${XDG_CONFIG_HOME}/environment.d"
environment_file=$( mktemp -d )/50-${package_name}.conf
<< EOF cat > "$environment_file"
DCMDICTPATH=${install_prefix}/share/dcmtk/dicom.dic
EOF
case "$install_prefix" in
  */home/*)
    install -D -m 0755 "$environment_file" "${XDG_CONFIG_HOME}/environment.d/50-${package_name}.conf"
    . ${hooks_dir}/post_install.sh
    ;;
  *)
    $SUDO install -D -m 0755 "$environment_file" "/etc/environment.d/50-${package_name}.conf"
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
