#! /usr/bin/make -rf
include _defaults.mk

tarball := ${HOME}/.cabal/packages/hackage.haskell.org/pandoc/${VERSION}/pandoc-${VERSION}.tar.gz

@install : @bin ${BASH_COMPLETION_USER_DIR}/completions/${package_name}

@bin : ${DOWNLOAD_DIR}/pandoc-3.1.3-1-amd64.deb
	${SUDO} dpkg -i "$<"

${DOWNLOAD_DIR}/pandoc-% :
	wget -P "$(@D)" \
		"https://github.com/jgm/pandoc/releases/download/${VERSION}/$(@F)"


%/completions/${package_name} %/bash_completion.d/${package_name} : | @bin
	pandoc --bash-completion | sed 's/\r$$//' > "$@"

%/pandoc.1 :
	tar -C "${TMPDIR}" -xaf "${tarball}" "pandoc-${VERSION}/man/pandoc.1"
	gzip -9 < "${TMPDIR}/pandoc-${VERSION}/man/pandoc.1" > "$@"
