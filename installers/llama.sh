#! /bin/sh
package_name=llama.cpp
anitya_project_id=328681
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
install_prefix=${packaging_prefix?}/llama
make -rf "llama.mk" "$source_dir"

mkdir -p ~/build/llama.cpp
cd ~/build/llama.cpp
	PATH="${PATH}:/usr/local/cuda/bin"
	cmake \
		-DCMAKE_INSTALL_PREFIX:PATH="$install_prefix" \
		-DCUDA_INCLUDE_DIRS=/usr/local/cuda/include \
		-DLLAMA_CUBLAS=ON \
		"$source_dir"
	cmake --build . --config Release --target install
cd -
