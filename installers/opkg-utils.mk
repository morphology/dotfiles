#! /usr/bin/make -rf
VERSION ?= 0.5.0
TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/opkg-% :
	curl -fL -o "$@" "https://git.yoctoproject.org/opkg-utils/snapshot/$(@F)"
