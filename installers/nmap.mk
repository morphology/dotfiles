#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.bz2
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/${package_name}-% :
	wget -P "$(@D)" \
		"https://nmap.org/dist/$(@F)"
