#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/curl-% :
	curl -fL -o "$@"     "https://curl.se/download/$(@F)" \
	         -o "$@.asc" "https://curl.se/download/$(@F).asc"
	-gpg --verify "$@.asc"
