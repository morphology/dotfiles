#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name} :
	mkdir -p "$@"
	git clone --depth=1 -b ${VERSION} \
		"https://github.com/tesseract-ocr/${package_name}.git" "$@"
	cd "$@" && sh autogen.sh && autoreconf -iv

%.traineddata :
	# Using "best", rather than "fast" or the legacy
	mkdir -p "$(@D)"
	wget -P "$(@D)" "https://github.com/tesseract-ocr/tessdata_best/raw/main/$(@F)"

# vim: ff=unix tabstop=3 shiftwidth=3 noexpandtab
