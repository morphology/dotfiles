#! /bin/sh
set -eu
stow_name="$1"
cd /usr/local/stow
	<< EOF cat > "${stow_name}/.stow-local-ignore"
AUTHORS
CONTRIBUTING.md
CONTRIBUTORS
LICENSE
PATENTS
README.md
SECURITY.md
VERSION
api
codereview.cfg
doc
go.env
misc
pkg
src
test
EOF
	stow -S --no-folding "$stow_name"
cd -
