TMPDIR := $(shell mktemp -d)

install : ${HOME}/bin/ngrok

${TMPDIR}/ngrok-%.zip :
	curl -o "$@" -fL "https://bin.equinox.io/c/4VmDzA7iaHb/$(@F)"

%/ngrok : ${TMPDIR}/ngrok-stable-linux-amd64.zip
	unzip -d "$(@D)" "$<"
	strip "$@"
