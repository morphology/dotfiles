#! /usr/bin/make -rf
# rad auth is needed for bootstrapping
package_name ?= radicle
SEED := https://seed.radicle.xyz/z3gqcJUoA1n9HaHKufZs5FCSGazv5.git

@install : ${HOME}/src/${package_name}
	sh ${package_name}.sh "$<"

%/src/radicle :
	git clone "${SEED}" "$@" --depth=1
