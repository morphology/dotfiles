#! /bin/sh
package_name=step-ca
# https://github.com/smallstep/certificates/releases
VERSION=0.24.2

. _hooks/setup.sh
make -rf "step.mk" @server

