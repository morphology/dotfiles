#! /usr/bin/env make -rf
VERSION ?= 1.6.3
TMPDIR := $(shell mktemp -d)

${HOME}/.local/net.downloadhelper.coapp-${VERSION} : ${TMPDIR}/net.downloadhelper.coapp-1.6.3-1_amd64.tar.gz
	tar -C "$(@D)" -xaf "$<"
	${PREFIX}/net.downloadhelper.coapp-1.6.3/bin/net.downloadhelper.coapp-linux-64 install --user

${TMPDIR}/net.downloadhelper.coapp-1.6.3-1_amd64.tar.gz :
	curl -fL -o "$@" "https://github.com/mi-g/vdhcoapp/releases/download/v${VERSION}/net.downloadhelper.coapp-${VERSION}-1_amd64.tar.gz"
