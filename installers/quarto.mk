#! /usr/bin/make -rf
VERSION ?= 1.3.361
# recommended:
install_prefix := ${HOME}/opt
include _defaults.mk


@install : ${DOWNLOAD_DIR}/quarto-${VERSION}-linux-amd64.tar.gz
	mkdir -p ${install_prefix}
	tar -C ${install_prefix} -xaf "$<"
	${install_prefix}/quarto-${VERSION}/bin/quarto check

${DOWNLOAD_DIR}/quarto-% :
	wget -P "$(@D)" \
		https://github.com/quarto-dev/quarto-cli/releases/download/v${VERSION}/$(@F)
