#! /bin/sh
package_name=vcpkg
VERSION=2023.04.15
set -e

. _hooks/setup.sh
build_dir=~/build/${package_name?}-${VERSION?}
make -rf "${package_name}.mk" "$build_dir"

cd "$build_dir"
  #./bootstrap-vcpkg.sh -disableMetrics
  ./vcpkg integrate install
  ./vcpkg integrate bash
cd -

if VCPKG_ROOT=$( cat ~/.vcpkg/vcpkg.path.txt ); then
  << EOF cat > ${XDG_CONFIG_HOME?}/environment.d/40-vcpkg.conf
VCPKG_ROOT=$VCPKG_ROOT
EOF
fi
