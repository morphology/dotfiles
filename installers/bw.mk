#! /usr/bin/make -rf
package_name := bw
VERSION != python3 -m helper.release_version latest 229011
include _defaults.mk


@install : ${DOWNLOAD_DIR}/bw-linux-${VERSION}.zip
	unzip -d "${TMPDIR}" "$<"
	install -D -m 0700 "${TMPDIR}/bw" "${HOME}/bin/"
	sh -x "${package_name}.sh"

${DOWNLOAD_DIR}/bw-linux-${VERSION}.zip :
	-xdg-open "https://vault.bitwarden.com/download/?app=cli&platform=linux"
	@read -rp "<Hit enter when downloaded>" REPLY
