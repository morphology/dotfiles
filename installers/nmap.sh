#! /bin/sh
package_name=nmap
anitya_project_id=2096
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}-${VERSION}
. ${hooks_dir}/pre_install.sh

cd "$source_dir"
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --without-zenmap
  make -j -l $(nproc)
  make install
cd -
#libtool --finish $install_prefix/lib

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
