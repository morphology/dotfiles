#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name} :
	git clone --depth=1 -b DCMTK-${VERSION} "https://git.dcmtk.org/$(@F).git" "$@"

%/src/fmjpeg2koj :
	git clone --depth=1 "https://github.com/DraconPern/$(@F).git" "$@"
