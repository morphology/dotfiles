#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/less-% :
	wget -P "$(@D)" \
		"https://www.greenwoodsoftware.com/less/$(@F)" \
		"https://www.greenwoodsoftware.com/less/less-${VERSION}.sig"
	-gpg --verify "$@.sig"

%/profile.d/lesspipe.sh %/.profile.d/lesspipe.sh :
	lesspipe > "$@"
