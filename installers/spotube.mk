#! /usr/bin/make -rf
package_name=spotube
include _defaults.mk

@upgrade : ${DOWNLOAD_DIR}/Spotube-linux-x86_64.deb
	gh api \
		--jq '.[0]["name"]' \
		-H "Accept: application/vnd.github+json" \
		-H "X-GitHub-Api-Version: 2022-11-28" \
		/repos/KRTirtho/${package_name}/releases
	${SUDO} apt install "$<"

${DOWNLOAD_DIR}/Spotube-% :
	wget -O "$@" --no-clobber \
		'https://github.com/KRTirtho/spotube/releases/latest/download/$(@F)'
