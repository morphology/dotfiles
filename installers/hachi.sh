#! /bin/sh
package_name=hachi
VERSION=1.2

set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}
. ${hooks_dir}/pre_install.sh

make -rf "${package_name?}.mk" "${source_dir}"
cd "${source_dir}"
	python3 -m venv .venv
	.venv/bin/pip install -r "requirements.txt"
cd -

