#! /usr/bin/make -rf

%/src/${package_name} :
	git clone -b v${VERSION} --depth=1 \
		"https://github.com/qpdf/${package_name}.git" "$@"

%/bash_completion.d/${package_name} %/bash-completion/completions/${package_name} :
	mkdir -p "$(@D)"
	qpdf --completion-bash > "$@"
