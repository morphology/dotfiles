#! /usr/bin/make -rf
include _defaults.mk

arch ?= amd64
VERSION ?= 5.17.11.3835

KEY_VERSION ?= 5-12-6


@install : ${DOWNLOAD_DIR}/zoom_${arch}.deb
	${SUDO} apt install "$<"

@key : ${DOWNLOAD_DIR}/package-signing-key-${KEY_VERSION}.gpg
	gpg --import "$<"

${DOWNLOAD_DIR}/zoom_%.deb ${DOWNLOAD_DIR}/zoom_%.rpm : | @key
	wget -P "$(@D)" \
		"https://zoom.us/client/${VERSION}/$(@F)"
	gpgv "$@"

${DOWNLOAD_DIR}/package-signing-key-%.gpg :
	wget -O- "https://zoom.us/linux/download/pubkey?version=${KEY_VERSION}" | \
		gpg --batch --dearmor > "$@"

# vim: noexpandtab ff=unix
