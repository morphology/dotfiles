#! /usr/bin/env make -rf

%/bin/${package_name} : ${HOME}/go/bin/${package_name}
	go install filippo.io/$(@F)/cmd/...@latest
	install -D -s ~/go/bin/$(@F) "$@"
	install -D -s ~/go/bin/age-keygen "$(@D)/age-keygen"

%/bin/age-plugin-yubikey :
	cargo install "$(@F)"
	install -D -s "${HOME}/.cargo/bin/$(@F)" "$@"

${HOME}/.pki/age/%.key :
	install -d "$(@D)"
	# might contain multiple keys
	age-keygen -o "$@"
