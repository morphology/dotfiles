VERSION ?= 1.3.1

TMPDIR := $(shell mktemp -d)

%/src/scrypt-${VERSION} : ${TMPDIR}/scrypt-${VERSION}.tgz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/scrypt-% :
	curl -fL -o "$@" "https://www.tarsnap.com/scrypt/$(@F)"
