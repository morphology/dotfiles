#! /usr/bin/make -rf
package_name = cloudflare-warp
include _defaults.mk
arch ?= amd64
.SECONDARY :

@install : /etc/apt/sources.list.d/${package_name}.list
	apt-get update

%/${package_name}.list : /usr/share/keyrings/${package_name}-archive-keyring.gpg
	$(file > $@,deb [arch=${arch} signed-by=$<] https://pkg.cloudflareclient.com/ ${codename} main)

%/keyrings/${package_name}-archive-keyring.gpg :
	curl -fL 'https://pkg.cloudflareclient.com/pubkey.gpg' | \
		gpg --no-default-keyring --keyring "$@" --import


%/Cloudflare_CA.crt %/Cloudflare_CA.pem :
	wget -P "$(@D)" 'https://developers.cloudflare.com/cloudflare-one/static/documentation/connections/$(@F)'
