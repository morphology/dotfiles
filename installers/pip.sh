#! /bin/sh
set -e
. _hooks/setup.sh
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  if ! $SUDO apt install \
    --no-install-suggests \
    --no-install-recommends \
    $dependencies; then
    echo "Skipping dependencies" >&2
  fi
fi
export -p > "pip.build-env"
make -rf python.mk ${HOME}/.local/bin/pip
