#! /usr/bin/make -rf

%/microsoft-archive-keyring.gpg :
	curl -fL "https://packages.microsoft.com/keys/microsoft.asc" | \
		gpg --dearmor > "$@"

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
