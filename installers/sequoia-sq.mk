#! /usr/bin/make -rf
package_name := sequoia-sq
VERSION != python3 -m helper.release_version latest 191617
include _defaults.mk

@install : ${HOME}/bin/sq

%/bin/sq :
ifneq (,${dependencies})
	${SUDO} apt-get install ${dependencies}
endif
	cargo install "${package_name}"
	install -Ds "${HOME}/.cargo/bin/sq" "$@"
