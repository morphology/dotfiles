#! /bin/sh
set -eu

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

systemctl --user restart gpg-agent.service
gpg --card-status
# pcsc_scan to see events
