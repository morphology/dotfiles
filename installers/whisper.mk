#! /usr/bin/make -rf
arch := x86_64
export HF_HUB_ENABLE_HF_TRANSFER = 1

$(shell mkdir -p models/)

models/ggml-% :
	huggingface-cli download --local-dir=$(@D) \
		ggerganov/whisper.cpp \
		"$(@F)"


%/src/whisper.cpp :
	git clone --depth=1 "https://github.com/ggerganov/$(@F)" "$@"

${DOWNLOAD_DIR}/cuda-keyring_%.deb :
	wget -P "$(@D)" \
		"https://developer.download.nvidia.com/compute/cuda/repos/wsl-ubuntu/${arch}/$(@F)"

