#! /usr/bin/make -rf
package_name := docker
include _defaults.mk
include /etc/os-release

os ?= linux/ubuntu

@install : /etc/apt/sources.list.d/docker.list
ifneq (,${dependencies})
	apt-get update
	apt-get install ${dependencies}
endif

%/docker.list : /etc/apt/keyrings/docker.gpg
	$(file > $@,deb [signed-by=$<] https://download.docker.com/${os} ${VERSION_CODENAME} stable)
	chmod +rx "$@"

%/keyrings/docker.gpg :
	curl -fL https://download.docker.com/${os}/gpg | \
		gpg --no-default-keyring --keyring="$@" --import
	chmod +rx "$@"
	
