#! /usr/bin/make -rf
include _defaults.mk

%/src/opencv %/src/opencv_contrib :
	git clone --depth 1 -b ${VERSION} \
		"https://github.com/opencv/$(@F).git" "$@"
