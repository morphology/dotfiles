#! /bin/sh
set -e
TMPDIR=$(mktemp -d)

desktop_file="$TMPDIR/${package_name}.desktop"
cat > "$desktop_file"

$SUDO desktop-file-install \
  --delete-original --rebuild-mime-info-cache \
  "$@" "$desktop_file"
