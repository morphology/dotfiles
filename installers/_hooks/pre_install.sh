# pre_install.sh
[ -n "$install_prefix" ] || install_prefix="${packaging_prefix?}/${package_name?}-${VERSION?}"
if [ -z "$source_dir" ]; then
  mkdir -p "${HOME}/src"
  source_dir=$( realpath -m "${HOME}/src/${package_name}-${VERSION}" )
fi

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  if ! $SUDO apt install \
    --no-install-suggests \
    --no-install-recommends \
    $dependencies; then
    echo "Skipping dependencies" >&2
  fi
fi

if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "${source_dir?}"
fi
export -p > "${package_name}.build-env"
