# setup.sh
ENV=$( realpath ".build-env" )
script_path=$( realpath "$0" )

# Basic dependencies
if dependencies=$( sed 's/[#].*//' "_setup.deps" ) && [ -n "$dependencies" ]; then
  case $(apt-mark showmanual $dependencies) in
    "") echo "Warning: missing dependencies: $dependencies";;
  esac >&2
fi

# Write persistent config:
if ! [ -e "$ENV" ]; then
	# Decide whether we install system-wide or personally
	if [ -n "$packaging_prefix" ]; then
		mkdir -p "$packaging_prefix"
	elif [ -w "/usr/local" ]; then
		umask 0002
		packaging_prefix="/usr/local/stow"
	else
		packaging_prefix="$HOME/.local/stow"
	fi >&2

	if ! DOWNLOAD_DIR=$( xdg-user-dir DOWNLOAD ); then
		DOWNLOAD_DIR="$HOME"
	fi
	if ! codename=$( lsb_release -sc ); then
		codename=bullseye
	fi

	<< EOF cat > .build-env
# Customized from $script_path
export codename="$codename"

# Modify any overrides to _defaults.mk here
export CURL_CA_BUNDLE
export DOWNLOAD_DIR="$DOWNLOAD_DIR"
export MAX_LOAD=$( nproc )
#export OPENSSL_ROOT_DIR=/usr/local
export SUDO=sudo
export hooks_dir="$( realpath _hooks )"
export packaging_prefix="$packaging_prefix"
#export vim_native_plugins="\${XDG_CONFIG_HOME}/nvim/pack/plugins/"
EOF
	echo "'$ENV' created"
	if [ -t 0 ]; then
		read -p "< Edit it and hit enter when ready >" REPLY
	fi
fi
. "$ENV"
[ -n "$version_query" ] || version_query=".latest_version"
if [ -z "$VERSION" ] && [ -n "$anitya_project_id" ]; then
	check_url="https://release-monitoring.org/api/v2/versions/?project_id=${anitya_project_id}"
	local_project_json="${package_name}-${anitya_project_id}.json"
	if ! [ -s "$local_project_json" ]; then
		wget -O "${local_project_json?}" "${check_url?}"
	fi
	if [ -s "$local_project_json" ]; then
		VERSION=$( jq -r "${version_query?}" < "${local_project_json}" | tr -d '\r' )
	fi
fi

export VERSION
export package_name
