# source this
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi
case $packaging_prefix in
  */stow)
    cd "$packaging_prefix"
      stow -S --no-folding $( basename "$install_prefix" )
    cd -
    ;;
esac

# vim: ff=unix
