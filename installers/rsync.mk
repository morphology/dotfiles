#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/rsync-% :
	wget -P "$(@D)" \
		"https://www.samba.org/ftp/rsync/src/$(@F)" \
	        "https://www.samba.org/ftp/rsync/src/$(@F).asc"
	-gpg --verify "$@.asc"
