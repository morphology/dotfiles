#! /bin/sh
package_name=zutty
VERSION=0.14

set -e

. _hooks/setup.sh
source_dir="${HOME}/src/zutty"
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/zutty
cd ~/build/zutty
  "$source_dir/waf" --top="$source_dir" configure \
    --prefix="$install_prefix"
  "$source_dir/waf" --top="$source_dir" install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
