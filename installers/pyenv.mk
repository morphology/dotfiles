#! /usr/bin/make -rf
PYENV_ROOT ?= ${HOME}/.pyenv

@install : ${PYENV_ROOT} ${HOME}/.profile.d/${package_name}.sh ${BASH_COMPLETION_USER_DIR}/completions/${package_name}

%/completions/${package_name} %/bash_completion.d/${package_name}.sh : | ${PYENV_ROOT}
	install -CD $|/completions/pyenv.bash "$@"

%/.profile.d/${package_name}.sh : | ${PYENV_ROOT}
	$(file>$@, PATH="$$PATH:$|/bin")
	$|/bin/pyenv init - >> "$@"

${PYENV_ROOT} :
	git clone --depth=1 https://github.com/pyenv/${package_name}.git ${PYENV_ROOT}
	git clone --depth=1 https://github.com/pyenv/pyenv-doctor.git ${PYENV_ROOT}/plugins/pyenv-doctor
	git clone --depth=1 https://github.com/pyenv/pyenv-update.git ${PYENV_ROOT}/plugins/pyenv-update
	git clone --depth=1 https://github.com/pyenv/pyenv-virtualenv.git ${PYENV_ROOT}/plugins/pyenv-virtualenv


