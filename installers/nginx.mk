#! /usr/bin/env make -rf

# Stable branch:
VERSION ?= 1.20.1

TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/nginx-% :
	curl -fL -o "$@"     "http://nginx.org/download/$(@F)" \
		 -o "$@.asc" "http://nginx.org/download/$(@F).asc"
	gpg --verify "$@.asc" "$@"

