#! /bin/sh
package_name=whisper.cpp
set -e

. _hooks/setup.sh
source_dir=${HOME}/src/${package_name}
install_prefix=${packaging_prefix}/whisper
pip install --user hf_transfer
. ${hooks_dir}/pre_install.sh

make -rf "whisper.mk" "$source_dir" models/ggml-base.en.bin

mkdir -p ~/build/whisper.cpp
cd ~/build/whisper.cpp
  # Incompatibilities between gcc-11 and CUDA 11.5:
  #    https://github.com/NVlabs/instant-ngp/issues/119
	PATH="${PATH}:/usr/local/cuda/bin"
	cmake \
		-DCMAKE_INSTALL_PREFIX:PATH="$install_prefix" \
		-DWHISPER_CUBLAS=1 \
		-GNinja \
		-DCMAKE_CUDA_HOST_COMPILER=g++-10 \
		"$source_dir"
	cmake --build . --config Release --target install -j
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
