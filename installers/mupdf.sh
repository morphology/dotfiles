#! /bin/sh
anitya_project_id=2034
package_name=mupdf
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}
. ${hooks_dir?}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"

cd "$source_dir"
  make HAVE_X11=yes HAVE_GLUT=yes prefix="$install_prefix" install
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
