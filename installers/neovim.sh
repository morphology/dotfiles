#! /bin/sh
set -eu
source_dir="$1"
install_prefix="$2"
MAX_LOAD=$(nproc)

# Builds within its source dir
cd "$source_dir"
  make \
    CMAKE_BUILD_TYPE=RelWithDebInfo \
    CMAKE_INSTALL_PREFIX="${install_prefix}" \
    -j -l $MAX_LOAD install
cd -
