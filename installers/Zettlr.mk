#! /usr/bin/make -rf
VERSION ?= 2.3.0
include _defaults.mk

@install : ${DOWNLOAD_DIR}/Zettlr-${VERSION}-amd64.deb
	${SUDO} apt install "$<"

${DOWNLOAD_DIR}/Zettlr-% :
	wget -P "$(@D)" "https://github.com/Zettlr/Zettlr/releases/download/v${VERSION}/$(@F)"
