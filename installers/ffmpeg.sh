#! /bin/sh
#
# You'll need nasm
# You might want nv-codec-headers
#
set -eu

source_dir="$1"
install_prefix="$2"

if ! command -v nasm; then
	echo "Install nasm first"
	exit 1
fi >&2

mkdir -p ~/build/ffmpeg
cd ~/build/ffmpeg
	${source_dir}/configure \
		--prefix="${install_prefix}" \
		--disable-static --as=yasm --enable-gpl \
		--enable-nonfree --enable-shared \
		--enable-chromaprint --enable-fontconfig \
		--enable-libcaca --enable-libfdk-aac \
		--enable-libfreetype --enable-libfontconfig --enable-libmp3lame --enable-libopus \
		--enable-libtheora --enable-libvorbis --enable-libvpx \
		--enable-libwebp --enable-libx264 --enable-libx265 --enable-libxml2 \
		--enable-cuda-nvcc --enable-libnpp \
		--extra-cflags=-I/usr/local/cuda/include --extra-ldflags=-L/usr/local/cuda/lib64  \
		--enable-openssl
	make -j -l $MAX_LOAD
	make install
cd -
