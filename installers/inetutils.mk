TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/inetutils-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/inetutils/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/inetutils/$(@F).sig"
	-gpg --verify "$@.sig"
