#! /bin/sh
package_name=nvidia-video-codec-sdk
VERSION=12.1.14
set -e

. _hooks/setup.sh
source_dir=${HOME}/src/Video_Codec_Interface_${VERSION?}
make -rf nvidia.mk "$source_dir"
# Total guess about expected location:
$SUDO install ${source_dir}/Interface/*.h /usr/include/