#! /bin/sh
package_name=cmake
anitya_project_id=306
set -e

version_query='.stable_versions | first'
. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh
#make -rf "${package_name?}.mk" "${source_dir?}"

build_dir=~/build/${package_name}-${VERSION}
mkdir -p "${build_dir?}"
cd "$build_dir"
  cmake "$source_dir" -DCMAKE_INSTALL_PREFIX="$install_prefix" || 
    "${source_dir}/bootstrap" --prefix="$install_prefix"
  make -j -l $MAX_LOAD
  make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
