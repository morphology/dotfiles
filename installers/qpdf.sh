#! /bin/sh
anitya_project_id=5542
package_name=qpdf
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
build_dir=~/build/${package_name}-${VERSION}
mkdir -p "$build_dir"
cd "$build_dir"
  cmake -B build \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    "$source_dir"
  cmake --build build
  cmake --install build
cd -

case "$packaging_prefix" in
  */home/*)
    . ${hooks_dir}/post_install.sh
    make -rf "${package_name}.mk" ~/.local/etc/bash_completion.d/${package_name}
    ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    $SUDO make -rf "${package_name}.mk" package_name=$package_name \
      /usr/share/bash-completion/completions/${package_name}
    ;;
esac
