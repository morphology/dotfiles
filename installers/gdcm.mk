#! /usr/bin/make -rf
include _defaults.mk

%/src/gdcm :
	git clone -b "v${VERSION}" --depth=1 \
		'https://github.com/malaterre/GDCM' "$@"

# vim: ff=unix noexpandtab
