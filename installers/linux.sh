#! /bin/sh
install-package unix
cd "$installer_dir"
  stow -S --no-folding "${package_name}"
cd -

<< EOF cat > ${XDG_CONFIG_HOME}/environment.d/90-browser.conf
BROWSER='sensible-browser %s'
EOF
