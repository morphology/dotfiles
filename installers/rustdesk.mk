#! /usr/bin/make -rf
arch := amd64

# Not open-source:
%/libsciter-gtk.so :
	wget -P "$(@D)" "https://raw.githubusercontent.com/c-smile/sciter-sdk/master/bin.lnx/x64/$(@F)"

%/src/rustdesk :
	git clone --depth=1 -b "${VERSION}" "https://github.com/rustdesk/rustdesk" "$@"
