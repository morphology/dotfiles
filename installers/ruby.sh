#! /bin/sh
package_name=ruby
anitya_project_id=4223
set -e

case $ruby_version in
  2|2.7) version_query='[.stable_versions[] | select(test("^2.7"))] | first' ;;
  3) version_query='.latest_version' ;;
esac
export ruby_version
. _hooks/setup.sh
source_dir=~/src/${package_name}-${VERSION}
export VERSION
. ${hooks_dir}/pre_install.sh

mkdir -p ~/src ~/build/ruby
cd ~/build/ruby
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=$HOME/etc \
    --enable-shared \
    --with-openssl-dir="${OPENSSL_ROOT_DIR?}"
  make -j -l $(nproc)
  make install
cd -
. ${hooks_dir}/post_root_install.sh

mkdir -p "${XDG_CONFIG_HOME}/environment.d"
<< EOF cat > "${XDG_CONFIG_HOME}/environment.d/50-${package_name}.conf"
RB_USER_INSTALL=true
EOF
