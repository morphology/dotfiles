#! /usr/bin/make -rf
include _defaults.mk

RVERSION=$(subst .,,${VERSION})

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/ghostscript-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"
	# Using local libraries:
	cd "$@" && rm -rf freetype lcms2mt jpeg libpng openjpeg tiff
	# https://www.linuxfromscratch.org/blfs/view/svn/pst/gs.html


${DOWNLOAD_DIR}/ghostscript-% :
	wget -P "$(@D)" "https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs${RVERSION}/ghostscript-${VERSION}.tar.gz"
	wget -P "$(@D)" "https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs${RVERSION}/SHA512SUMS"
	cd "$(@D)" && sha512sum --ignore-missing -c SHA512SUMS

# vim: ff=unix tabstop=3 shiftwidth=3 noexpandtab
