#! /bin/sh
package_name=openssh
anitya_project_id=2565
set -e

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/openssh
cd ~/build/openssh
  umask 0002
  $SUDO install -d /var/lib/sshd /var/empty
  getent group sshd || $SUDO groupadd -r sshd
  getent passwd sshd || $SUDO useradd -r -g sshd -c 'sshd privsep' -m -d /var/empty -s /bin/false sshd
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --sysconfdir=/etc/ssh \
    --with-privsep-user=sshd \
    --with-privsep-path=/var/lib/sshd \
    --with-pid-dir=/run \
    --with-ssl-dir="${OPENSSL_ROOT_DIR?}" \
    --with-security-key-builtin
  make all -j -l $(nproc)
  $SUDO make install
  $SUDO chmod a+rX -R "$install_prefix"
cd -

