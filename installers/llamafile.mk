#! /usr/bin/make -rf

%/src/${package_name} :
	git clone --depth 1 -b "${VERSION}" "https://github.com/Mozilla-Ocho/$(@F)" "$@"
