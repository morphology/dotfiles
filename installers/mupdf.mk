#! /usr/bin/make -rf
TMPDIR := $(shell mktemp -d)

%/src/${package_name} :
	git clone --depth 1 -b ${VERSION} --recursive "git://git.ghostscript.com/${package_name}.git" "$@"
