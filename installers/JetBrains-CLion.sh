#! /bin/sh
export VERSION=2022.1
set -eu
export TMPDIR=$( mktemp -d )
install_prefix="/opt/clion-$VERSION"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$install_prefix"
fi

cat > "${TMPDIR}/${package_name}.desktop" << EOF
[Desktop Entry]
Type=Application
Version=1.0
Name=CLion ${VERSION}
Exec="${install_prefix}/bin/clion.sh" %u
Terminal=false
Categories=Development;IDE;Java;
EOF

desktop-file-install --dir="${XDG_DATA_HOME}/applications" \
  --delete-original --copy-name-to-generic-name \
  --set-icon="${install_prefix}/bin/clion.svg" \
  "${TMPDIR}/${package_name}.desktop"

desktop-file-install --dir="${XDG_DATA_HOME}/applications" \
  --rebuild-mime-info-cache --copy-name-to-generic-name \
  "${install_prefix}/bin/cmake/linux/share/applications/cmake-gui.desktop"
