#! /bin/sh
package_name=less
VERSION=633
anitya_project_id=1550
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/less
cd ~/build/less
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --sysconfdir=/etc \
    --with-editor=sensible-editor
  make all -j -l $(nproc)
  make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
