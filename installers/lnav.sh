#! /bin/sh
package_name=lnav
anitya_project_id=1833
version_query='[.stable_versions[] | select(test("test") | not)] | first'
set -e

[ -n "$XDG_STATE_HOME" ] || XDG_STATE_HOME=${HOME}/.local/state

. _hooks/setup.sh
source_dir=~/src/${package_name}
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/lnav
cd ~/build/lnav
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=/etc
  # Somehow -j -l can still lead to runaway load here
  make
  make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
