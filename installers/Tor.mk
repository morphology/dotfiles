VERSION ?= 12.0.4
TMPDIR := $(shell mktemp -d)

install : ${HOME}/tor-browser

%/tor-code-signing.asc :
	curl -fL -o "$@" "https://tor.net/media/tor-code-signing.asc"

${TMPDIR}/tor-browser-% :
	curl -fL \
		-o "$@"     "https://www.torproject.org/dist/torbrowser/${VERSION}/$(@F)" \
		-o "$@.asc" "https://www.torproject.org/dist/torbrowser/${VERSION}/$(@F).asc"
	-gpg --verify "$@.asc" "$@"

%/tor-browser : ${TMPDIR}/tor-browser-linux64-${VERSION}_ALL.tar.xz
	mkdir -p "$@"
	tar -C "$(@D)" -xaf "$<"


