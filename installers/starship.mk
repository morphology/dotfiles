#! /usr/bin/make -rf
package_name := starship
include _defaults.mk

@install : ${HOME}/bin/starship ${HOME}/.profile.d/${package_name}.sh ${BASH_COMPLETION_USER_DIR}/completions/${package_name}

%/.profile.d/${package_name}.sh : | ${HOME}/bin/starship
	mkdir -p "$(@D)"
	starship init bash --print-full-init > "$@"

%/completions/${package_name} %/bash_completion.d/${package_name} : | ${HOME}/bin/starship
	mkdir -p "$(@D)"
	starship completions bash > "$@"


%/bin/starship :
ifneq (,${dependencies})
	${SUDO} apt-get install --no-install-recommends --no-install-suggests \
		${dependencies}
endif
	cargo install starship
	install -Ds ~/.cargo/bin/starship "$@"
