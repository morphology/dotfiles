TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/coreutils-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/coreutils/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/coreutils/$(@F).sig"
	-gpg --verify "$@.sig"
