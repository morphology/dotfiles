#! /usr/bin/make -rf
package_name := golang
VERSION != python3 -m helper.release_version latest_stable 1227
include _defaults.mk
arch = linux-amd64

@install : /usr/local/stow/go-${VERSION}
	sh ${package_name}-stow.sh "$(<F)"

%/go-${VERSION} : | ${DOWNLOAD_DIR}/go${VERSION}.${arch}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" --transform="s#^go/#$(@F)/#" -xaf $|

${DOWNLOAD_DIR}/go% :
	curl -fL \
		-o "$@" "https://golang.org/dl/$(@F)"
