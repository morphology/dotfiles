#! /usr/bin/env make -rf
TMPDIR := $(shell mktemp -d)

%/src/Orthanc-${VERSION} : ${TMPDIR}/Orthanc-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/Orthanc-% :
	curl -fL -o "$@" "https://www.orthanc-server.com/downloads/get.php?path=/orthanc/$(@F)"
