#! /bin/sh
set -eu

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

cd "$installer_dir"
  stow -S "$package_name"
cd -
