#! /bin/sh
package_name=busybox
install_prefix=/usr/local
anitya_project_id=230
set -e

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/busybox
cp -b "${package_name}-config/${VERSION}" ~/build/busybox/.config || echo "< Expecting manual config >"
cd ~/build/busybox
  if ! [ -e .config ]; then
    [ -t 0 ] && make KBUILD_SRC="${source_dir}" -f "${source_dir}/Makefile" menuconfig
  fi
  install -d "${install_prefix}/bin"
  make -j -l $(nproc)
  if command -v upx; then
    upx -o "${install_prefix}/bin/busybox" busybox
  else
    install -s busybox "${install_prefix}/bin/busybox"
  fi
cd -
