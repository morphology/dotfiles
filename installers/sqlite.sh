#! /bin/sh
package_name=sqlite
anitya_project_id=4877
set -e

. _hooks/setup.sh
VERSION=$(python3.7 -c "from packaging.version import parse; v=parse('${VERSION}'); print(f'{v.major}{v.minor:02d}{v.micro:02d}00')")
source_dir=~/src/${package_name?}-autoconf-${VERSION?}
. ${hooks_dir?}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
build_dir=~/build/${package_name}-${VERSION}
mkdir -p "$build_dir"
cd "$build_dir"
	CFLAGS="-DSQLITE_ENABLE_COLUMN_METADATA=1" \
	"$source_dir/configure" \
		--prefix="$install_prefix" \
		--localstatedir="$XDG_STATE_HOME" \
		--runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
		--sysconfdir=$HOME/etc
	make -j -l $(nproc)
	make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*)
		. ${hooks_dir}/post_root_install.sh
		$SUDO ldconfig
		;;
esac
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
