#! /usr/bin/make -rf
VERSION ?= 23.9.0.24
include _defaults.mk
keystore := /opt/Citrix/ICAClient/keystore


@install : ${DOWNLOAD_DIR}/icaclient_${VERSION}_amd64.deb
	${SUDO} apt install "$^"
	-${SUDO} systemctl disable --now ctxlogd
	-${SUDO} systemctl disable --now ctxcwalogd

${DOWNLOAD_DIR}/icaclient_23.3.0.32_% :
	# Download from 'https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html'"
	$(file >>$(@D)/SHA256SUMS, 5efe13960c1b5b4de6e2993cb81bb482d9cfaac5c2d96030ed589426743a8892 *icaclient_23.3.0.32_amd64.deb)
	cd "$(@D)" && sha256sum --ignore-missing -c SHA256SUMS

${DOWNLOAD_DIR}/icaclient_23.9.0.24_% :
	# Download from 'https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html'"
	$(file >>$(@D)/SHA256SUMS, 68b5c131119ca52afd82e89dabbcb3ef4381c2652c06cb58d4fc341dd8772961 *icaclient_23.9.0.24_amd64.deb)
	cd "$(@D)" && sha256sum --ignore-missing -c SHA256SUMS

%.pem :
	printf '\n' | openssl s_client $*:443 > "$@"

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
