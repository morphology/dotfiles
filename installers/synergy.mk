#! /usr/bin/make -rf
# TODO: check for Wayland support
VERSION = 1.14.6-snapshot.88fdd263
include _defaults.mk

@install : ${DOWNLOAD_DIR}/synergy_${VERSION}_ubuntu22_amd64.deb
	-${SUDO} dpkg -i "$<"
	${SUDO} apt install -f
