#! /bin/sh
package_name=vimwiki
. _hooks/setup.sh

make -rf "${package_name}.mk"
mkdir -p "${HOME}/.profile.d"
if ! [ -e ${HOME}/.profile.d/vimwiki ]; then
  << EOF cat > ${HOME}/.profile.d/vimwiki
VIMWIKI_EDITOR=nvim
EOF
fi
