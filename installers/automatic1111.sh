#! /bin/sh
export package_name=automatic1111
set -e

. _hooks/setup.sh

source_dir=~/src/${package_name?}
make -rf "${package_name}.mk" "$source_dir"

pip install wheel
pip install hf_transfer torch torchvision
python3 -c 'import torch; assert torch.cuda.is_available()'
pip freeze

mkdir -p ~/etc
<< EOF cat > ~/etc/automatic1111.sh
#! /bin/sh
cd "$source_dir"
# Default port is 7860
HF_HUB_ENABLE_HF_TRANSFER=1 python3 launch.py \\
	--api \\
	--theme dark \\
	--update-all-extensions \\
	--update-check \\
	--listen 
EOF
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
