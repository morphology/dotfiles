
install : /usr/share/bash-completions/completions/${package_name}

%/bash-completions/completions/poetry :
	poetry completions -n bash > "$@"
