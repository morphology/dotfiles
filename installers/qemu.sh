#! /bin/sh
package_name=qemu
anitya_project_id=13607
set -e

. _hooks/setup.sh
for colliding_package in qemu qemu-kvm qemu-user qemu-user-static; do
  $SUDO apt-mark hold "$colliding_package" || echo "Ignoring $colliding_package"
done

source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/qemu
cd ~/build/qemu
  "${source_dir}/configure" \
    --prefix="$install_prefix" \
    --localstatedir="${XDG_STATE_HOME}/${package_name}" \
    --sysconfdir=/usr/local/etc/ \
    --target-list=x86_64-linux-user \
    --disable-sdl \
    --enable-plugins \
    --enable-curl \
    --enable-curses \
    --enable-dbus-display \
    --enable-guest-agent \
    --enable-kvm \
    --enable-libssh \
    --enable-png \
    --enable-spice \
    --enable-spice-protocol \
    --enable-libusb \
    --enable-zstd \
    --enable-tools \
    --enable-vnc \
    --enable-vnc-jpeg \
  make -j -l $(nproc)
  $SUDO install -g staff -m 0775 -d /usr/local/etc/qemu
  make install
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO "${source_dir}/scripts/qemu-binfmt-conf.sh" --systemd x86_64 --qemu-path=/opt/bin/qemu
    $SUDO systemctl restart systemd-binfmt
    ;;
esac
