#! /bin/sh
export VERSION='9.1'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/coreutils
cd build/coreutils
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --enable-single-binary=symlinks \
    --disable-nls \
    --with-linux-crypto \
    --with-openssl=optional
  
  make all -j -l $(nproc)
  make install
  if command -v upx; then
    upx "${install_prefix}/bin/coreutils"
  fi
cd -
