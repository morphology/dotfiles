#! /usr/bin/make -rf
TMPDIR := $(shell mktemp -d)

%/src/${package_name} :
	git clone --depth=1 -b "gdb-${VERSION}-release" "git://sourceware.org/git/binutils-gdb.git" "$@"
	cd "$@" ; make distclean
