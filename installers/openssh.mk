#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/openssh-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/openssh-% :
	wget -P "$(@D)" \
		"https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$(@F)" \
		"https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$(@F).asc" \
		"https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/RELEASE_KEY.asc"
	-gpg --verify "$@.asc"
