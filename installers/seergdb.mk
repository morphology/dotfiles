#! /usr/bin/make -rf
VERSION = 1.16

%/src/${package_name} :
	git clone --depth 1 -b v${VERSION} "https://github.com/epasveer/seer.git/" "$@"
