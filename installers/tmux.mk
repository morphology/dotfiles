#! /usr/bin/make -rf
XDG_CONFIG_HOME ?= ${HOME}/.config
XDG_DATA_HOME ?= ${HOME}/.local/share
BASH_COMPLETION_USER_DIR ?= ${XDG_DATA_HOME}/bash-completion

%/src/${package_name} :
	git clone --depth=1 -b ${VERSION} https://github.com/tmux/${package_name}.git "$@"
	cd "$@" ; sh autogen.sh

%/bash_completion.d/${package_name} %/completions/${package_name} :
	wget -P "$(@D)" "https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux"
