#! /usr/bin/make -rf
# https://rustdesk.com/docs/en/self-host/install/
VERSION := 1.1.7
RVERSION := 1.1.7-4
arch := amd64

TMPDIR := $(shell mktemp -d)

install : ${TMPDIR}/rustdesk-server-hbbr_${VERSION}_${arch}.deb ${TMPDIR}/rustdesk-server-hbbs_${VERSION}_${arch}.deb ${TMPDIR}/rustdesk-server-utils_${VERSION}_${arch}.deb
	${SUDO} dpkg -i $^

${TMPDIR}/rustdesk-server-%.deb :
	curl -fL \
		-o "$@" "https://github.com/rustdesk/rustdesk-server/releases/download/${RVERSION}/$(@F)"
