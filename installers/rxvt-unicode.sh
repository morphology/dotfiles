#! /bin/sh
package_name=rxvt-unicode
anitya_project_id=4750
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/rxvt-unicode
cd ~/build/rxvt-unicode
  "${source_dir}/configure" \
    --prefix="${install_prefix?}" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=$HOME/etc \
    --enable-256-color \
    --enable-xft \
    --enable-transparency \
    --enable-fading \
    --enable-next-scroll \
    --enable-keepscrolling \
    --enable-selectionscrolling \
    --enable-mousewheel \
    --enable-slipwheeling \
    --enable-smart-resize \
    --disable-perl
  make -j -l $(nproc)
  make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO update-alternatives --install \
      /usr/bin/x-terminal-emulator x-terminal-emulator \
      "${packaging_prefix}/../bin/urxvtc" 25
    if [ -t 1 ]; then
      $SUDO update-alternatives --config x-terminal-emulator
    fi
    ;;
esac
case $(gsettings get org.gnome.desktop.default-applications.terminal exec) in
  urxvt*) ;;
  *)
    gsettings set org.gnome.desktop.default-applications.terminal exec \
      urxvtc
    ;;
esac
