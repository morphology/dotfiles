#! /bin/sh
package_name=llamafile
VERSION=0.4
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"

cd "$source_dir"
	make -j -l $(nproc)
	make install PREFIX="$install_prefix"
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
