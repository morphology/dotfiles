#! /usr/bin/make -rf
include _defaults.mk

TMPDIR := $(shell mktemp -d)

%/src/git-${VERSION} : ${DOWNLOAD_DIR}/git-${VERSION}.tar
	mkdir -p $(@D)
	tar -C $(@D) -xf $<
	cd "$@" && autoreconf -fi

${DOWNLOAD_DIR}/git-%.tar : | @keys
	# git signs the .tar contents before compression
	wget -P "$(@D)" \
		"https://mirrors.edge.kernel.org/pub/software/scm/git/$(@F).xz" \
		"https://mirrors.edge.kernel.org/pub/software/scm/git/$(@F).sign"
	xz -d "$@.xz"
	gpgv "$@.sign" "$@"


@keys : ${TMPDIR}/pgpkeys-master.tar.gz
	tar -C "${TMPDIR}" -xaf "$<"
	gpg --import ${TMPDIR}/pgpkeys-master/keys/*.asc

${TMPDIR}/pgpkeys-master.tar.gz :
	# from the git repo https://git.kernel.org/pub/scm/docs/kernel/pgpkeys.git
	wget -P "$(@D)" \
		"https://git.kernel.org/pub/scm/docs/kernel/pgpkeys.git/snapshot/$(@F)"
