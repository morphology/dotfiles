#! /usr/bin/make -rf
package_name=FlyingCarpet
include _defaults.mk

@upgrade : ${DOWNLOAD_DIR}/linux_flying-carpet_8.0.1_amd64.deb
	gh api \
		--jq '.[0]["name"]' \
		-H "Accept: application/vnd.github+json" \
		-H "X-GitHub-Api-Version: 2022-11-28" \
		/repos/spieglt/${package_name}/releases
	${SUDO} apt install "$<"

${DOWNLOAD_DIR}/% :
	wget -O "$@" --no-clobber \
		'https://github.com/spieglt/FlyingCarpet/releases/latest/download/$(@F)'
	dpkg-deb --show "$@"

