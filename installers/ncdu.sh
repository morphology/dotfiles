#! /bin/sh
package_name=ncdu
anitya_project_id=6045
version_query='[.stable_versions[] | select(test("^1[.]"))] | first'
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}
. "${hooks_dir}/pre_install.sh"

[ -n "$XDG_STATE_HOME" ] || XDG_STATE_HOME="${HOME}/.local/state"
[ -n "$XDG_RUNTIME_DIR" ] || XDG_RUNTIME_DIR=/run/user/$( id -u )
mkdir -p ~/build/ncdu
cd ~/build/ncdu
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --disable-dependency-tracking \
    --with-ncurses
  make all -j -l $(nproc)
  make install
cd -
. "${hooks_dir}/install_ready.sh"
case "$packaging_prefix" in
  */home/* ) . "${hooks_dir}/post_install.sh" ;;
  *        ) . "${hooks_dir}/post_root_install.sh" ;;
esac
