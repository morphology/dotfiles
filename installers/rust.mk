#! /usr/bin/make -rf
package_name := rust
VERSION != python3 -m helper.release_version latest 7635
include _defaults.mk
dist_platform = x86_64-unknown-linux-gnu


@install : ${HOME}/build/rust-${VERSION}-${dist_platform}
	sh $</install.sh --disable-ldconfig --prefix="/usr/local/stow/${package_name}-${VERSION}"
	sh "${package_name}-stow.sh" ${package_name}-${VERSION}

%/build/rust-${VERSION}-${dist_platform} : ${DOWNLOAD_DIR}/rust-${VERSION}-${dist_platform}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/rust-% : ${CURDIR}/${package_name}.gpg
	curl -fL \
		-o "$@"     "https://static.rust-lang.org/dist/$(@F)" \
	        -o "$@.asc" "https://static.rust-lang.org/dist/$(@F).asc"
	gpgv --keyring "$<" "$@.asc" "$@"

%/rust.gpg :
	#gpg --auto-key-locate nodefault,wkd \
	#	--locate-keys 85AB96E6FA1BE5FE
	curl -fL "https://static.rust-lang.org/rust-key.gpg.ascii" | \
		gpg --no-default-keyring --keyring "$@" --import

