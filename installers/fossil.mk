#! /usr/bin/make -rf
TMPDIR := $(shell mktemp -d)

%/src/fossil-src : ${TMPDIR}/fossil-src.tar.gz
	tar -C "$(@D)" -xaf "$<"

%/src/fossil-src-${VERSION} : ${TMPDIR}/fossil-src-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/fossil-src.% :
	curl -fL -o "$@" "https://fossil-scm.org/home/tarball/$(@F)?name=fossil-src&uuid=trunk"
