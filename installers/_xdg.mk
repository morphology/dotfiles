#! /usr/bin/make -rf

XDG_CACHE_HOME ?= ${HOME}/.cache
XDG_CONFIG_HOME ?= ${HOME}/.config
XDG_DATA_HOME ?= ${HOME}/.local/share
XDG_RUNTIME_DIR ?= /run/user/$(shell id -u)
XDG_STATE_HOME ?= ${HOME}/.local/state
DOWNLOAD_DIR != xdg-user-dir DOWNLOAD

${XDG_CONFIG_HOME}/user-dirs.dirs :
	mkdir -p "$(@D)"
	-xdg-user-dirs-update
