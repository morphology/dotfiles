#! /bin/sh
export VERSION=0.10
set -eu
export install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/$package_name" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

mkdir -p build/minisign
cd build/minisign
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    $source_dir
  make -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  make -rf "${package_name}.mk" ~/.minisign/minisign.key
cd -
