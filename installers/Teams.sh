#! /bin/sh
package_name=Teams
set -e
make -rf "${package_name}.mk" /usr/share/keyrings/microsoft-archive-keyring.gpg
<< EOF cat > /etc/apt/sources.list.d/teams.list
deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-archive-keyring.gpg] https://packages.microsoft.com/repos/ms-teams stable main
EOF

apt update
apt install teams
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
