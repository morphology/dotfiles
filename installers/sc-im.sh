#! /bin/sh
VERSION=0.8.3
package_name=sc-im
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}
. ${hooks_dir?}/pre_install.sh

cd "${source_dir}/src"
  make prefix="$install_prefix"
  make install prefix="$install_prefix"
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
