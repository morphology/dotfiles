#! /usr/bin/make -rf
NODE_MAJOR := 20

TMPDIR ?= $(shell mktemp -d)

/etc/apt/sources.list.d/nodesource.list : /etc/apt/keyrings/nodesource.gpg
	$(file >> $@,deb [signed-by=$<] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main)

%/keyrings/nodesource.gpg :
	wget "https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key"
	gpg --import --no-default-keyring --keyring="$@" nodesource-repo.gpg.key
