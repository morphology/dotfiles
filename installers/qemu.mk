#! /bin/sh

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" "https://gitlab.com/qemu-project/$(@F).git" "$@"

