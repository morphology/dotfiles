#! /usr/bin/make -rf
VERSION ?= 7.3.4
include _defaults.mk

/opt/google/earth/pro/googleearth : ${DOWNLOAD_DIR}/google-earth-pro-stable_${VERSION}_amd64.deb
	${SUDO} dpkg -i $^

${DOWNLOAD_DIR}/%.deb :
	wget -P "$(@D)" \
		"https://dl.google.com/dl/linux/direct/$(@F)"

