#! /usr/bin/make -rf

TMPDIR := $(shell mktemp -d)

%/build/${package_name}-${VERSION} : ${TMPDIR}/coredns_${VERSION}_linux_amd64.tgz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"

${TMPDIR}/coredns_% :
	curl -fL -o "$@" "https://github.com/coredns/coredns/releases/download/v${VERSION}/$(@F)"
