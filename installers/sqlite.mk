#! /usr/bin/make -rf
include _defaults.mk

# year of tarball release:
YEAR ?= $(shell date +%Y)

%/src/sqlite-autoconf-${VERSION} : ${DOWNLOAD_DIR}/sqlite-autoconf-${VERSION}.tar.gz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/sqlite-% :
	curl -fl -o "$@" "https://www.sqlite.org/${YEAR}/$(@F)"
