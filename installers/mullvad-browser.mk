#! /usr/bin/make -rf
package_name = mullvad-browser
VERSION = 12.5.1

DOWNLOAD_DIR ?= $(shell xdg-user-dir DOWNLOAD)

@install : ${HOME}/${package_name}

%/${package_name} : ${DOWNLOAD_DIR}/mullvad-browser-linux64-${VERSION}_ALL.tar.xz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

@key :
	curl https://openpgpkey.torproject.org/.well-known/openpgpkey/torproject.org/hu/kounek7zrdx745qydx6p59t9mqjpuhdf | gpg --import

${DOWNLOAD_DIR}/mullvad-% : | @key
	wget -P "$(@D)" \
		https://cdn.mullvad.net/browser/${VERSION}/$(@F) \
		https://cdn.mullvad.net/browser/${VERSION}/$(@F).asc
	gpgv "$@.asc" "$@"

