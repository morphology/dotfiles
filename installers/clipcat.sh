#! /bin/sh
set -eu
source_dir=$( realpath "src/${package_name}" )

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi

type fzf || install-package fzf

cd $source_dir
  cargo build --release --features=all
  install -s -t "${HOME}/bin" \
    "target/release/clipcatctl" \
    "target/release/clipcatd" \
    "target/release/clipcat-menu" \
    "target/release/clipcat-notify"
cd -

cd "$installer_dir"
  make -rf "${package_name}.mk" \
    $XDG_CONFIG_HOME/clipcat/clipcatd.toml \
    $XDG_CONFIG_HOME/clipcat/clipcatctl.toml \
    $XDG_CONFIG_HOME/clipcat/clipcat-menu.toml \
    $HOME/.local/etc/bash_completion.d/clipcatd \
    $HOME/.local/etc/bash_completion.d/clipcatctl \
    $HOME/.local/etc/bash_completion.d/clipcat-menu
cd -
