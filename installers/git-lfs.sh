#! /bin/sh
anitya_project_id=11551
package_name=git-lfs
set -e

. _hooks/setup.sh
make -rf "${package_name}.mk" install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
