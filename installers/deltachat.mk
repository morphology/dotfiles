#! /usr/bin/make -rf
include _defaults.mk
VERSION ?= 1.42.2
arch := amd64

@install : ${DOWNLOAD_DIR}/deltachat-desktop_${VERSION}_${arch}.deb
	${SUDO} apt install "$<"

${DOWNLOAD_DIR}/deltachat-%.deb ${DOWNLOAD_DIR}/DeltaChat% :
	wget -P "$(@D)" \
		"https://download.delta.chat/desktop/v${VERSION}/$(@F)"

# vim: ff=unix tabstop=3 noexpandtab
