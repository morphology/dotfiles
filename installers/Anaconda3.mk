#! /usr/bin/make -rf
package_name := Anaconda3
VERSION = 2024.06-1
include _defaults.mk
dist_platform = Linux-x86_64


@install : ${DOWNLOAD_DIR}/Miniforge3-${dist_platform}.sh
	sh '$<' -bu
	ln -s "${HOME}/miniforge3/etc/profile.d/mamba.sh" "${HOME}/.profile.d/"

${DOWNLOAD_DIR}/Anaconda3-% :
	wget -P '$(@D)' 'https://repo.anaconda.com/archive/$(@F)'
	sha256sum '$@'
	read -p "Hit enter after verifying this against 'https://repo.anaconda.com/archive/'" REPLY

${DOWNLOAD_DIR}/Miniforge3-% :
	wget -P '$(@D)' 'https://github.com/conda-forge/miniforge/releases/latest/download/$(@F)'
