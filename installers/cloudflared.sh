#! /bin/sh
package_name=cloudflared

. _hooks/setup.sh
$SUDO make -rf "${package_name}.mk" codename="${codename}"
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install \
    --no-install-suggests \
    --no-install-recommends \
    $dependencies
fi
