#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name} :
	git clone -b openssl-${VERSION} --depth=1 \
		"git://git.openssl.org/${package_name}.git" "$@"

@key :
	gpg --keyserver hkps://keys.openpgp.org --recv-keys DC7032662AF885E2F47F243F527466A21CA79E6D

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/openssl-% : | @key
	curl -fL -o "$@"        "https://www.openssl.org/source/$(@F)" \
		 -o "$@.sha256" "https://www.openssl.org/source/$(@F).sha256" \
		 -o "$@.asc"    "https://www.openssl.org/source/$(@F).asc"
	gpg --verify "$@.asc"
