#! /usr/bin/make -rf

TMPDIR := $(shell mktemp -d)

%/src/ruby-${VERSION} : ${TMPDIR}/ruby-${VERSION}.tar.xz
	mkdir -p "$(@D)"
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/ruby-2.7% :
	curl -fL -o "$@" "https://cache.ruby-lang.org/pub/ruby/2.7/$(@F)"

${TMPDIR}/ruby-3.2% :
	curl -fL -o "$@" "https://cache.ruby-lang.org/pub/ruby/3.2/$(@F)"
