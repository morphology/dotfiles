#! /usr/bin/make -rf
include _defaults.mk

%/src/Python-${VERSION} : ${DOWNLOAD_DIR}/Python-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/Python-% :
	wget -P "$(@D)" \
		"https://www.python.org/ftp/python/${VERSION}/$(@F)"


${HOME}/.local/bin/pip : ${DOWNLOAD_DIR}/pip.pyz
	python3 "$<" install --user --upgrade pip

${DOWNLOAD_DIR}/pip.pyz :
	wget -O "$@" "https://bootstrap.pypa.io/pip/$(@F)"
