#! /usr/bin/make -rf
package_name = cloudflared
include _defaults.mk
arch ?= amd64
.SECONDARY :

@install : /etc/apt/sources.list.d/${package_name}.list
	apt-get update

%/${package_name}.list : /usr/share/keyrings/cloudflare-main.gpg
	$(file > $@,deb [arch=${arch} signed-by=$<] https://pkg.cloudflare.com/cloudflared ${codename} main)

%/keyrings/cloudflare-main.gpg :
	curl -fL 'https://pkg.cloudflare.com/cloudflare-main.gpg' | \
		gpg --no-default-keyring --keyring "$@" --import
