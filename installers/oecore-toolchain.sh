#! /bin/sh
VERSION=6.0
set -eu
install_prefix="/opt/stow/${package_name}-${VERSION}"

DOWNLOAD_DIR=$( xdg-user-dir DOWNLOAD )
chmod +x "${DOWNLOAD_DIR}/oecore-x86_64-core2-64-toolchain-${VERSION}.sh"

install -d "$install_prefix" "${XDG_CONFIG_HOME}/bash"
sh "${DOWNLOAD_DIR}/oecore-x86_64-core2-64-toolchain-${VERSION}.sh" \
  -d "$install_prefix"
<< EOF cat > "${XDG_CONFIG_HOME}/bash/${package_name}.bashrc"
. ${install_prefix}/environment-setup-core2-64-nilrt-linux
EOF
