#! /usr/bin/env make -rf
TMPDIR := $(shell mktemp -d)

%/julia-${VERSION} : ${TMPDIR}/julia-${VERSION}-linux-x86_64.tar.gz
	tar -C "$(@D)" -xaf "$<"

%/juliareleases.asc :
	curl -fL -o "$@" "https://julialang.org/assets/$(@F)"

${TMPDIR}/julia-1.6% :
	curl -f -o "$@" "https://julialang-s3.julialang.org/bin/linux/x64/1.6/$(@F)"

${TMPDIR}/julia-1.7% :
	curl -f -o "$@" "https://julialang-s3.julialang.org/bin/linux/x64/1.7/$(@F)"
