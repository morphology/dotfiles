#! /bin/sh
export VERSION=1.6.3
set -eu
install_prefix="${HOME}/.local"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "${install_prefix}"
fi
if [ -s "${package_name}.stow-local-ignore" ]; then
  install -C -D ${package_name}.stow-local-ignore ${install_prefix}/.stow-local-ignore
fi

install -d "$XDG_CONFIG_HOME/sh"
cat > "$XDG_CONFIG_HOME/sh/${package_name}" << EOF
PATH="\$PATH:$install_prefix/net.downloadhelper.coapp-1.6.3/converter/build/linux/64"
EOF
