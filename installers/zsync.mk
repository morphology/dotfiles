#! /usr/bin/make -rf
TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/zsync-${VERSION}.tar.bz2
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/zsync-% :
	curl -fL -o "$@" "http://zsync.moria.org.uk/download/$(@F)"
