#! /usr/bin/make -rf
TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/at_${VERSION}.orig.tar.gz
	tar -C "$(@D)" -xaf "$<"
	patch $@/configure.ac < at.patch
	cd "$@" ; autoreconf -fi


${TMPDIR}/at_% :
	curl -fL -o "$@" "http://software.calhariz.com/at/$(@F)" \
		 -o "$@.sig" "http://software.calhariz.com/at/$(@F).sig"
	-gpg --verify "$@.sig"
