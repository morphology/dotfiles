TMPDIR := $(shell mktemp -d)

%/src/tar-${VERSION} : ${TMPDIR}/tar-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/tar-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/tar/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/tar/$(@F).sig"
	-gpg --verify "$@.sig"
