#! /usr/bin/make -rf
include _defaults.mk


%/src/nv-codec-headers :
	git clone --depth 1 -b n${VERSION} \
		"https://git.videolan.org/git/ffmpeg/$(@F).git" "$@"

%/src/Video_Codec_Interface_${VERSION} : ${DOWNLOAD_DIR}/Video_Codec_Interface_${VERSION}.zip
	unzip -d "$(@D)" "$<"

${DOWNLOAD_DIR}/Video_Codec_Interface_%.zip :
	xdg-open "https://developer.nvidia.com/downloads/designworks/video-codec-sdk/secure/12.1/$(@F)"
	@read -rp "<Hit enter when downloaded>" REPLY
