#! /usr/bin/make -rf

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" "https://github.com/andmarti1424/$(@F).git" "$@"
