#! /bin/sh
package_name=libptytty
anitya_project_id=239552
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/libptytty
cd ~/build/libptytty
  cmake -DCMAKE_INSTALL_PREFIX="${install_prefix?}" \
    -DCMAKE_BUILD_TYPE=Release \
    -DPT_UTMP_FILE:STRING=/run/utmp \
    "$source_dir"
  make -j -l $(nproc)
  make install
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    $SUDO ldconfig
    ;;
esac
