#! /usr/bin/env make -rf

%/src/${package_name} :
	git clone "https://github.com/xrelkd/$(@F).git" "$@"

${XDG_CONFIG_HOME}/clipcat/%.toml :
	install -d "$(@D)"
	$* > "$@"

${HOME}/.local/etc/bash_completion.d/% :
	install -d "$(@D)"
	$* completions bash > "$@"
