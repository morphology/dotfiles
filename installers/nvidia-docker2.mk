#! /usr/bin/make -rf
package_name := nvidia-container-toolkit
include _defaults.mk

@install : /etc/apt/sources.list.d/${package_name}.list
ifneq (,${dependencies})
	apt-get install --no-install-suggests --no-install-recommends \
		${dependencies}
endif
	apt-get update
	apt-get install nvidia-docker2
	nvidia-ctk runtime configure --runtime=docker

%/sources.list.d/nvidia-container-toolkit.list : /etc/apt/keyrings/${package_name}-keyring.gpg
	$(file > $@,deb [signed-by=$<] https://nvidia.github.io/libnvidia-container/stable/deb/$$(ARCH) /)

%/keyrings/${package_name}-keyring.gpg :
	curl -fL 'https://nvidia.github.io/libnvidia-container/gpgkey' | \
		gpg --dearmor -o "$@"
