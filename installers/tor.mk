#! /usr/bin/env make -rf
package_name := tor
include _defaults.mk


@upgrade : /etc/apt/sources.list.d/${package_name}.list
ifneq (,${UBUNTU_DEPS})
	apt-get install apt-transport-https
	apt-get update
	apt-get install --no-install-suggests --no-install-recommends ${UBUNTU_DEPS} -d
endif
	install -o tor -g tor -d /var/lib/tor /var/lib/tor/onion_auth

/etc/apt/sources.list.d/tor.list : /usr/share/keyrings/tor-archive-keyring.gpg
	$(file > $@,deb [arch=${arch} signed-by=$<] https://deb.torproject.org/torproject.org stable main)
	$(file >> $@,deb-src [arch=${arch} signed-by=$<] https://deb.torproject.org/torproject.org stable main)


%/tor-archive-keyring.gpg :
	curl -fL 'https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc' | \
		gpg --dearmor -o "$@"
