#! /bin/sh
package_name=tesseract
anitya_project_id=4954
set -e

. _hooks/setup.sh
source_dir="${HOME}/src/${package_name}"
. ${hooks_dir}/xdg.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/tesseract
cd ~/build/tesseract
	${source_dir}/configure \
		--prefix="$install_prefix" \
		--localstatedir="$XDG_STATE_HOME" \
		--runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
		--sysconfdir=$HOME/etc \
		--disable-legacy
	make -j -l $MAX_LOAD
	make install
cd -
. ${hooks_dir}/install_ready.sh
make -rf "${package_name}.mk" "${install_prefix}/share/tessdata/eng.traineddata"
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*)
		. ${hooks_dir}/post_root_install.sh
		$SUDO ldconfig
		;;
esac

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
