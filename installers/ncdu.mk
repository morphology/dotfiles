#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" \
		'https://code.blicky.net/yorhel/${package_name}.git' "$@"
	cd "$@" ; autoreconf -fi
