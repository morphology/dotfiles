# Installation on Windows

[Visual Studio Community](https://visualstudio.microsoft.com/vs/community/) with:
	C++ core features
	MSVC v143 - VS 2022 C++ x64/x86
	C++ CMake tools for Windows
	Windows 11 SDK

Then,
	winget install -e --id Nvidia.CUDA

Optionally,
	winget install -e --id cURL.cURL
	winget install -e --id frippery.busybox-w32
	winget install -e --id jqlang.jq
	winget install -e --id Python.Python.3.11

For all compilations, note the [compilation
variables](https://github.com/ggerganov/llama.cpp#cublas)

Setup environment,

	Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
	$Env:PATH += ";C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\CMake\CMake\bin;C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\Git\mingw64\bin;C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.38.33130\bin\Hostx64\x86"

# Installation on WSL2

### Dependencies for GPU passthrough

[CUDA on WSL](https://docs.nvidia.com/cuda/wsl-user-guide/index.html) directs you to [specific installation](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=WSL-Ubuntu&target_version=2.0&target_type=deb_local).

	make -rf llama.mk ${DOWNLOAD_DIR}/cuda-keyring_1.1-1_all.deb
	dpkg -i ${DOWNLOAD_DIR}/cuda-keyring_1.1-1_all.deb
	echo apt-get -y install --no-install-suggests --no-install-recommends \
		cuda-toolkit-12-3


### Virtual subnet setup

1. Add proxy

	netsh interface portproxy add v4tov4 listenport=8080 connectport=8080 connectaddress=172...

2. Open firewall

	netsh advfirewall firewall add rule name= "Open Port 8080" dir=in action=allow protocol=TCP localport=8080

3. Tear down proxy

	netsh interface portproxy delete v4tov4 listenport=8080

4. Tear down firewall rule

	netsh advfirewall firewall delete rule name="Open port 8080"


# vim: ft=md
