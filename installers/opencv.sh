#! /bin/sh
#
# ffmpeg
# nvidia-video-codec-sdk
#
anitya_project_id=6615
package_name=opencv
set -e

. _hooks/setup.sh
source_dir=${HOME}/src/${package_name?}
make -rf "${package_name}.mk" "${HOME}/src/opencv_contrib"
. ${hooks_dir?}/pre_install.sh

mkdir -p ~/build/OpenCV
cd ~/build/OpenCV
  cmake -DCMAKE_INSTALL_PREFIX=${install_prefix?} \
       -DBUILD_SHARED_LIBS=OFF \
       -DBUILD_TESTS=OFF \
       -DBUILD_PERF_TESTS=OFF \
       -DBUILD_EXAMPLES=OFF \
       -DWITH_OPENEXR=OFF \
       -DWITH_CUDA=ON \
       -DWITH_CUBLAS=ON \
       -DWITH_CUDNN=ON \
       -DWITH_NVCUVID=ON \
       -DWITH_NVCUVENC=ON \
       -DOPENCV_DNN_CUDA=ON \
       -DCMAKE_BUILD_TYPE=Release \
       -DOPENCV_EXTRA_MODULES_PATH="${HOME}/src/opencv_contrib/modules" \
       "$source_dir"
	make -j -l $MAX_LOAD
	make install
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*)
		. ${hooks_dir}/post_root_install.sh
		$SUDO ldconfig
		;;
esac
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
