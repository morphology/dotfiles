#! /bin/sh
package_name=rustdesk
anitya_project_id=236374
set -e

. _hooks/setup.sh
source_dir=~/src/rustdesk

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
${VCPKG_ROOT?}/vcpkg install \
  libvpx \
  libyuv \
  opus
export -p > "${package_name}.build-env"
make -rf "${package_name}.mk" "$source_dir" "${source_dir}/target/debug/libsciter-gtk.so"

cd "$source_dir"
  cargo run
cd -
