#! /bin/sh
set -eu

stow_name="$1"

cd /usr/local/stow
	${SUDO} stow -S --no-folding "$stow_name"
	${SUDO} ldconfig
cd -
