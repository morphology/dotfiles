#! /usr/bin/make -rf
package_name := fzf
include _defaults.mk

@install :
ifneq (,${dependencies})
	${SUDO} apt-get update
	${SUDO} apt-get install ${dependencies}
endif
	install -C -D "/usr/share/doc/fzf/examples/key-bindings.bash" \
		"${HOME}/.profile.d/${package_name}-key-bindings.sh"
	make -C '${XDG_CONFIG_HOME}/nvim/pack' \
		'junegunn/start/${package_name}' \
		'theniceboy/start/fzf-gitignore'
