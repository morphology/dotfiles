TMPDIR := $(shell mktemp -d)

%/src/make-${VERSION} : ${TMPDIR}/make-${VERSION}.tar.lz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/make-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/make/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/make/$(@F).sig"
	-gpg --verify "$@.sig"
