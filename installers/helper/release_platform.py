import platform
import shlex


def get_default_content():
    content = """\
#! /usr/bin/make -rf
.DEFAULT: @install
.PHONY: @install @upgrade @user
.SECONDARY :

install_prefix ?= ${HOME}
_ := $(realpath $(firstword $(MAKEFILE_LIST)))

GH_PAGER=
MAX_LOAD != nproc
include _xdg.mk
BASH_COMPLETION_USER_DIR ?= ${XDG_DATA_HOME}/bash-completion
BASH_COMPLETION_DIR ?= /etc/bash_completion.d/
vim_native_plugins ?= ${XDG_CONFIG_HOME}/nvim/pack/plugins/

TMPDIR != mktemp -d
SUDO ?= sudo
export

ifneq (,${package_name})
	dependencies != sed 's/[\#].*//' "${package_name}.deps"
endif
arch != dpkg --print-architecture

"""
    for name, value in platform.freedesktop_os_release().items():
        content += f"OS_{name} := {shlex.quote(value)}\n"
    return content


if __name__ == "__main__":
    print(get_default_content())
