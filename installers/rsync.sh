#! /bin/sh
package_name=rsync
anitya_project_id=4217
set -e

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/rsync
cd ~/build/rsync
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="$XDG_RUNTIME_DIR/${package_name}" \
    --sysconfdir=$HOME/etc \
    --enable-openssl="${OPENSSL_ROOT_DIR?}" \
    --with-protected-args
  
  make all -j -l $(nproc)
  make install
cd -

case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
