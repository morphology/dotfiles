#! /usr/bin/make -rf
include _defaults.mk

@install : ${vim_native_plugins}/start/${package_name} ${vim_native_plugins}/start/${package_name}/doc


%/start/${package_name} :
	git clone --depth=1 "https://github.com/vimwiki/${package_name}.git" "$@"

%/vimwiki/doc :
	nvim -c "helptags $@" -c quit
