#! /bin/sh
cd "$installer_dir"
  install-package emacs
  make -rf doom-emacs.mk
  stow --no-folding -S doom-emacs
cd -

