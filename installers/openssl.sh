#! /bin/sh
package_name=openssl
anitya_project_id=2566
#version_query='[.stable_versions[] | select(test("^1"))] | first'
set -e

. _hooks/setup.sh
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/openssl
cd ~/build/openssl
  umask 0002
  # enable-fips unsupported on v1
  "$source_dir/config" \
    --prefix="$install_prefix" \
    --openssldir="/usr/local/ssl" \
    enable-fips \
    shared \
    zlib-dynamic
  make -j -l $(nproc)
  case ${install_prefix} in
    */home/* ) make install ;;
    *)
      $SUDO install -m 0755 -d /usr/local/ssl
      $SUDO make install
      $SUDO chmod a+rwX -R "$install_prefix"
      ;;
  esac
cd -
case ${install_prefix} in
  */home/*) case "${packaging_prefix}" in
    /*stow/*) echo "Use LD_LIBRARY_PATH=${packaging_prefix}/../lib:${packaging_prefix}/../lib64:\${LD_LIBRARY_PATH}" ;;
    * ) echo "Use LD_LIBRARY_PATH=${install_prefix}/lib:${install_prefix}/lib64:\${LD_LIBRARY_PATH}" ;;
    esac ;;
  */stow/*) << EOF cat > "${install_prefix}/.stow-local-ignore"
lib/libcrypto.a
lib/libcrypto.so
lib/libssl.a
lib/libssl.so
lib64/libcrypto.a
lib64/libcrypto.so
lib64/libssl.a
lib64/libssl.so
EOF
    echo "You might need to adjust ld.so.conf and then run ldconfig"
    ;;
esac
