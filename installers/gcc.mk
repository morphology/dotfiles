TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/gcc-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/gcc/gcc-${VERSION}/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/gcc/gcc-${VERSION}/$(@F).sig"
	-gpg --verify "$@.sig"
