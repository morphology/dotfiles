#! /bin/sh
cd "$installer_dir"
  make -rf "${project_name}.mk" ~/.nodenv
cd -

cd ~/.nodenv
  src/configure
  make -C src
cd -
