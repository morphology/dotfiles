#! /bin/sh -eu
package_name=pandoc
anitya_project_id=2589
set -e

. _hooks/setup.sh
if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi
make -rf "${package_name}.mk"

