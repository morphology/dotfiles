#! /usr/bin/env make -rf
package_name := githubcli
include _defaults.mk

@install : /etc/apt/sources.list.d/${package_name}.list
ifneq (,${dependencies})
	apt-get install apt-transport-https
	apt-get update
	apt-get install ${dependencies}
endif

/etc/apt/sources.list.d/githubcli.list : /etc/apt/trusted.gpg.d/githubcli-archive-keyring.gpg
	$(file > $@,deb [arch=${arch} signed-by=$<] https://cli.github.com/packages stable main)


%/githubcli-archive-keyring.gpg :
	curl -fL "https://cli.github.com/packages/$(@F)" | \
		gpg --no-default-keyring --keyring="$@" --import
	chmod 0644 '$@'


@user : ${HOME}/.local/etc/bash_completion.d/${package_name}

%/bash_completion.d/${package_name} :
	install -d "$(@D)"
	gh completion -s bash > $@
