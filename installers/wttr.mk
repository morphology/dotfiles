#! /usr/bin/make -rf

@install : ${HOME}/bin/wttr.sh

%/wttr.sh :
	wget -P "$(@D)" -O wttr.sh \
		"https://raw.githubusercontent.com/chubin/wttr.in/master/share/bash-function.txt"
	chmod +x "$@"
