#! /usr/bin/make -rf
package_name := neovim
VERSION != python3 -m helper.release_version latest_stable 9037
include _defaults.mk

@install : /usr/src/${package_name}
ifneq (,${dependencies})
	${SUDO} apt-get update
	${SUDO} apt-get install --no-install-suggests --no-install-recommends \
		${dependencies}
endif
	sh "${package_name}.sh" "$<" /usr/local/stow/${package_name}-${VERSION}
	sh "${package_name}-stow.sh" ${package_name}-${VERSION}

%/src/${package_name} :
	git clone --depth=1 -b "v${VERSION}" \
		"https://github.com/neovim/${package_name}.git" "$@"

%/src/neovim-qt :
	git clone --depth 1 \
		"https://github.com/equalsraf/neovim-qt.git" "$@"
