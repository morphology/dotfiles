#! /bin/sh
set -eu
install_prefix="${packaging_prefix}/${package_name}"
source_dir="/usr/src/${package_name}"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install --no-install-suggests --no-install-recommends $dependencies
fi

if [ -s "${package_name}.mk" ]; then
  make -rf "${package_name}.mk" "$source_dir"
fi
cd "$source_dir"
  git submodule init
  git submodule update --recursive
cd -

mkdir -p build/libjxl
cd build/libjxl
  clang --version
  cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
    -DBUILD_SHARED_LIBS:BOOL=OFF \
    -DBUILD_TESTING=OFF \
    -DINSTALL_GTEST:BOOL=OFF \
    -DJPEGXL_ENABLE_BENCHMARK:BOOL=OFF \
    -DJPEGXL_ENABLE_PLUGINS:BOOL=ON \
    -DJPEGXL_WARNINGS_AS_ERRORS:BOOL=OFF \
    $source_dir/
  cmake --build . -j$(nproc)
  cmake --install .
cd -

case $packaging_prefix in
  */stow|/opt/${SCHROOT_CHROOT_NAME})
    cd "$packaging_prefix"
      stow -S --no-folding "${package_name}"
    cd -
    ;;
esac
