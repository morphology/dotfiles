#! /bin/sh
# GitHub Actions static checker
package_name=actionlint
anitya_project_id=312546
set -e

. _hooks/setup.sh
make -rf "${package_name}.mk" install

