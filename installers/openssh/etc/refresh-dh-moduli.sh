#! /bin/sh

ssh-keygen -G moduli-2048.candidates -b 2048
pv -el moduli-2048.candidates | ssh-keygen -T moduli
