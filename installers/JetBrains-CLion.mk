#! /usr/bin/make -rf
include _defaults.mk

%.tar.gz :
	@echo "Download '$@' from 'https://www.jetbrains.com/clion/download/'"

%/clion-${VERSION} : ${DOWNLOAD_DIR}/CLion-${VERSION}.tar.gz
	cd "$(<D)" && sha256sum -c "$<.sha256"
	tar -C "$(@D)" -xaf "$<"
