VERSION ?= 12.0.4
TMPDIR := $(shell mktemp -d)

install : ${HOME}/mullvad-browser

%/mullvad-code-signing.asc :
	curl -fL -o "$@" "https://mullvad.net/media/mullvad-code-signing.asc"

${TMPDIR}/mullvad-browser-% :
	curl -fL \
		-o "$@"     "https://browser-mirror-3.mullvad.net/browser/${VERSION}/$(@F)" \
		-o "$@.asc" "https://browser-mirror-3.mullvad.net/browser/${VERSION}/$(@F).asc"
	-gpg --verify "$@.asc" "$@"

%/mullvad-browser : ${TMPDIR}/mullvad-browser-linux64-${VERSION}_ALL.tar.xz
	mkdir -p "$@"
	tar -C "$(@D)" -xaf "$<"


