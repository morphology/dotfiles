#! /usr/bin/make -rf
include _defaults.mk
arch = $(shell uname -m)

/opt/${package_name}/ : ${DOWNLOAD_DIR}/${package_name}_${VERSION}_Linux_${arch}.tar.gz
	install -m 0755 -d "$@"
	tar -C "$(@D)" -xaf "$<"

/var/lib/${package_name}/navidrome.toml :
	$(file >$@,MusicFolder = "/home/Music")

%/${package_name}.service :
	${SUDO} install -C -D "$(@F)" "$@"
	${SUDO} systemctl daemon-reload

${DOWNLOAD_DIR}/navidrome_% :
	wget -P "$(@D)" \
		"https://github.com/navidrome/navidrome/releases/download/v${VERSION}/$(@F)"
