%/src/${package_name} :
	git clone --depth=1 "https://github.com/libjxl/${package_name}.git" --recursive "$@"
