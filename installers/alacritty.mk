#! /usr/bin/make -rf
include _defaults.mk


@client : ${HOME}/.terminfo/a/alacritty ${HOME}/.tmux.conf.d/${package_name}.tmux


%/src/${package_name} :
	git clone -b v${VERSION} --depth=1 \
		https://github.com/alacritty/${package_name}.git "$@"


${HOME}/.terminfo/a/alacritty : ${TMPDIR}/alacritty.info
	mkdir -p "$(@D)"
	tic -xe alacritty,alacritty.direct "$<"


%/${package_name}.info :
	mkdir -p "$(@D)"
	wget -O- "https://raw.githubusercontent.com/alacritty/${package_name}/master/extra/$(@F)" > "$@"

${HOME}/.tmux.conf.d/${package_name}.tmux :
	mkdir -p "$(@D)"
	$(file>$@,set-option -g default-terminal alacritty)

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
