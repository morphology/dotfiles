#! /usr/bin/make -rf
include _defaults.mk

@install : @bin ${BASH_COMPLETION_USER_DIR}/completions/${package_name}

@bin : ${DOWNLOAD_DIR}/step-cli_${VERSION}_amd64.deb
	${SUDO} apt install "$<"

@server : ${DOWNLOAD_DIR}/step-ca_${VERSION}_amd64.deb
	${SUDO} apt install "$<"


${DOWNLOAD_DIR}/step-cli_% :
	wget -P "$(@D)" \
		"https://dl.smallstep.com/gh-release/cli/docs-cli-install/v${VERSION}/$(@F)" \
		"https://dl.smallstep.com/gh-release/cli/docs-cli-install/v${VERSION}/$(@F).pem" \
		"https://dl.smallstep.com/gh-release/cli/docs-cli-install/v${VERSION}/$(@F).sig"
	cosign verify-blob \
		--certificate $@.pem \
		--signature $@.sig \
		--certificate-identity-regexp "https://github\.com/smallstep/cli/.*" \
		--certificate-oidc-issuer "https://token.actions.githubusercontent.com" \
		$@

${DOWNLOAD_DIR}/step-ca_% :
	wget -P "$(@D)" \
		"https://dl.smallstep.com/gh-release/certificates/docs-ca-install/v${VERSION}/$(@F)" \
		"https://dl.smallstep.com/gh-release/certificates/docs-ca-install/v${VERSION}/$(@F).pem" \
		"https://dl.smallstep.com/gh-release/certificates/docs-ca-install/v${VERSION}/$(@F).sig"
	cosign verify-blob \
		--certificate $@.pem \
		--signature $@.sig \
		--certificate-identity-regexp "https://github\.com/smallstep/certificates/.*" \
		--certificate-oidc-issuer "https://token.actions.githubusercontent.com" \
		$@

%/completions/${package_name} : | @bin
	mkdir -p "$(@D)"
	step completion bash > "$@"
