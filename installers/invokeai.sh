#! /bin/sh
export package_name=invokeai
set -e

. _hooks/setup.sh
install_prefix="${HOME}/invokeai"
export INVOKEAI_ROOT="$install_prefix"

if dependencies=$( sed 's/[#].*//' "${package_name}.deps" ) && [ -n "$dependencies" ]; then
  $SUDO apt install \
    --no-install-suggests \
    --no-install-recommends \
    $dependencies
fi


pip install wheel
pip install hf_transfer torch torchvision torchaudio OpenCV-Python pypatchmatch
python -c 'import torch; assert torch.cuda.is_available()'
pip freeze
pip install "InvokeAI[xformers]" --use-pep517 \
	--extra-index-url https://download.pytorch.org/whl/cu121

if ! [ -d "$INVOKEAI_ROOT" ]; then
	invokeai-configure --root "$INVOKEAI_ROOT"
fi

<< EOF cat > ${install_prefix?}/invokeai.sh
#! /bin/sh
# Default port is 9090
export HF_HUB_ENABLE_HF_TRANSFER=1
export INVOKEAI_ROOT="$INVOKEAI_ROOT"
invokeai-web --root="$INVOKEAI_ROOT" \\
	--host 0.0.0.0 \\
	"\$@"
EOF
# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
