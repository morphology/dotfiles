#! /bin/sh
set -eu
stow_name="$1"
cd /usr/local/stow
	stow -S --no-folding "$stow_name"
	${SUDO} update-alternatives --install /usr/bin/editor editor \
		$(realpath -s ../bin/nvim) 99
cd -
