#! /bin/sh
export package_name=pyenv
. $PWD/.build-env
export BASH_COMPLETION_USER_DIR
make -rf "${package_name}.mk"

cd ~/.pyenv
  src/configure
  make -C src
cd -
