#! /bin/sh
package_name=nebula
anitya_project_id=89343
case $( uname -m ) in
	i686) arch=linux-386 ;;
	x86_64) arch=linux-amd64 ;;
esac
export arch
set -e

. _hooks/setup.sh
# Custom pre_install
[ -n "$install_prefix" ] || install_prefix="${packaging_prefix}/${package_name}-${VERSION}"

make -rf "${package_name}.mk" ~/build/${package_name}-${VERSION}
cd ~/build/${package_name}-${VERSION}
	getent group nebula || $SUDO groupadd -r nebula
	getent passwd nebula || $SUDO useradd -d /run -g nebula -s /usr/sbin/nologin -r nebula
	install -g staff -d "${install_prefix}/sbin/"
	install -D -g staff nebula nebula-cert "${install_prefix}/sbin/"
	$SUDO setcap cap_net_admin=+pe "${install_prefix}/sbin/nebula"
cd -

if ! [ -e "${XDG_CONFIG_HOME}/systemd/user/nebula@.service" ]; then
	<< EOF cat > "${XDG_CONFIG_HOME}/systemd/user/nebula@.service"
[Unit]
Description=Nebula Service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=5
WorkingDirectory=%E/${package_name}
ExecStart=${install_prefix}/sbin/nebula -config %i.yml

[Install]
WantedBy=default.target
EOF
	systemctl --user daemon-reload
fi

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
