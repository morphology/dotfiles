#! /bin/sh
package_name=RustRover
VERSION=233.13135.116
set -e

. _hooks/setup.sh
install_prefix="/opt/RustRover-$VERSION"
make -rf JetBrains-RustRover.mk "$install_prefix"

<< EOF sh ${hooks_dir}/write_desktop_file.sh \
	--copy-name-to-generic-name \
	--dir=/usr/local/share/applications \
	--set-icon="${install_prefix}/bin/rustrover.svg" \
	--vendor=jetbrains
[Desktop Entry]
Type=Application
Version=1.0
Name=RustRover
Exec="${install_prefix}/bin/rustrover.sh" %u
Terminal=false
Categories=Development;IDE;Java;
EOF
