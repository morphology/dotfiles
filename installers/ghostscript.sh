#! /bin/sh
package_name=ghostscript
anitya_project_id=1157
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

cd "$source_dir"
	"${source_dir}/configure" \
		--prefix="${install_prefix?}" \
		--localstatedir="$XDG_STATE_HOME" \
		--runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
		--sysconfdir=$HOME/etc \
		--with-drivers=FILES \
		--with-openjpeg=/usr \
		--with-system-libtiff
	make -j -l $(nproc)
	make -j -l $(nproc) so
	make install
	install -d "${install_prefix}/lib"
	install -D "${source_dir}/demos/python/gsapi.py" "${install_prefix}/etc"
	cp -d "${source_dir}"/sobin/* "${install_prefix}/lib"
cd -
. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
	*/stow/*) if ! [ -e "${packaging_prefix}/share/tessdata/eng.traineddata" ]; then
			make -rf "tesseract.mk" "${install_prefix}/share/tessdata/eng.traineddata"
		else
			echo "Skipping '${packaging_prefix}/share/tessdata/eng.traineddata"
		fi
		;;
esac
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*)
		. ${hooks_dir}/post_root_install.sh
		$SUDO ldconfig
		;;
esac

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
