#! /bin/sh
package_name=python
anitya_project_id=13254
set -e

if [ -z "$VERSION" ]; then
  VERSION=3.10
fi
version_query="[.stable_versions[] | select(test(\"^${VERSION?}\"))] | first"
unset VERSION

[ -n "$XDG_RUNTIME_DIR" ] || XDG_RUNTIME_DIR=/run/user/$( id -u )
[ -n "$XDG_STATE_HOME" ] || XDG_STATE_HOME=${HOME}/.local/state

. _hooks/setup.sh
source_dir=~/src/Python-${VERSION?}
if [ -z "$install_prefix" ]; then
  if [ -d "${HOME}/2.pyenv" ]; then
    mkdir -p "${HOME}/.pyenv/versions"
    install_prefix="${HOME}/.pyenv/versions/${VERSION}"
  else
    install_prefix="/usr/local/stow/python-${VERSION}"
  fi
fi


case "$VERSION" in
  2.*)
    if dependencies=$( sed 's/[#].*//' "python-2.deps" ) && [ -n "$dependencies" ]; then
      if ! $SUDO apt install --no-install-suggests --no-install-recommends $dependencies; then
        echo "Skipping dependencies"
      fi
    fi
    ;;
  3.*)
    if dependencies=$( sed 's/[#].*//' "${codename}/python.deps" ) && [ -n "$dependencies" ]; then
      if ! $SUDO apt install --no-install-suggests --no-install-recommends $dependencies; then
        echo "Skipping dependencies"
      fi
    fi
    ;;
esac

make -rf "${package_name}.mk" "$source_dir"
install -d "${install_prefix}"
case "$VERSION:$packaging_prefix" in
  2.*:*/stow)
    << EOF cat > "${install_prefix}/.stow-local-ignore"
bin/2to3
bin/easy_install
bin/idle
bin/pip2?
bin/pydoc
bin/python
bin/python2
bin/python2-config
bin/python-config
lib/python2[.][0-9]+
lib/libpython2[.]so
share/man/man1/python2[.]1
EOF
    ;;
  3.*:*/stow)
    << EOF cat > ${install_prefix}/.stow-local-ignore
bin/2to3
bin/idle3
bin/pip3?
bin/pydoc3
bin/python
bin/python3
bin/python3-config
bin/pyvenv
lib/python3[.][0-9]+
lib/libpython3[.]so
lib/pkgconfig
share/man/man1/python3[.]1
EOF
    ;;
esac

case "$install_prefix" in
  */home/*)
    localstatedir="${XDG_STATE_HOME?}"
    runstatedir="${XDG_RUNTIME_DIR?}"
    sysconfdir="${HOME?}/etc"
    ;;
  *)
    localstatedir=/var
    runstatedir=${localstatedir}/run
    sysconfdir=/etc
    ;;
esac
export -p > "${package_name}.build-env"

mkdir -p ~/build/python-$VERSION
cd ~/build/python-$VERSION
  # For example, --with-dtrace, --with-valgrind, --enable-profiling, --enable-pystats
  case $VERSION in
    2.*)
      $source_dir/configure \
        --prefix="$install_prefix" \
        --localstatedir="$localstatedir" \
        --sysconfdir="$sysconfdir" \
        --enable-shared \
        --with-system-expat \
        --with-system-ffi
      ;;
    3.7.*|3.8.*|3.9.*)
      # --with-openssl="${OPENSSL_ROOT_DIR?}"
      $source_dir/configure \
        --prefix="$install_prefix" \
        --localstatedir="$localstatedir" \
        --sysconfdir="$sysconfdir" \
        --enable-shared \
        --enable-loadable-sqlite-extensions=yes \
        --with-system-expat \
        --with-system-ffi \
        --with-system-libmpdec
      ;;
    *)  # 3.10+
      # --with-openssl="${OPENSSL_ROOT_DIR?}"
      $source_dir/configure \
        --prefix="$install_prefix" \
        --localstatedir="$localstatedir" \
        --runstatedir="$runstatedir" \
        --sysconfdir="$sysconfdir" \
        --enable-shared \
        --enable-loadable-sqlite-extensions=yes \
        --with-system-expat \
        --with-system-ffi \
        --with-system-libmpdec
      ;;
  esac
  make -j -l $(nproc)
  make install
cd -
export LD_LIBRARY_PATH="${install_prefix}/lib:${LD_LIBRARY_PATH}"
case "$VERSION:$install_prefix" in
  2.*:*/home/*)
    "${install_prefix}/bin/python2" -m ensurepip
    "${install_prefix}/bin/python2" -m pip install virtualenv
    ;;
  3.*:*/home/*)
    "${install_prefix}/bin/python3" -m ensurepip
    ;;
esac
if ! install -D "${source_dir}/Tools/gdb/libpython.py" "${HOME}/etc/python-${VERSION}-gdb.py"; then
  echo "No GDB helper in this version" >&2
fi

# vim: ff=unix
