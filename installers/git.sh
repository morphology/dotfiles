#! /bin/sh
package_name=git
anitya_project_id=5350
misc_dir=$( mktemp -d )
set -e

. _hooks/setup.sh
. ${hooks_dir?}/pre_install.sh

cd "$source_dir"
  make -B configure
  # This is meant for very reduced systems which might need a build of curl
  # --with-curl=/usr/local
  ./configure \
    --prefix="$install_prefix" \
    --localstatedir=/var \
    --runstatedir=/run \
    --sysconfdir=/etc \
    --with-editor=/usr/bin/sensible-editor \
    --with-pager=/usr/bin/sensible-pager
  make -j -l $(nproc) all
  if ! make doc info install-doc install-html install-info; then
    echo "Skipping documentation" >&2
  fi
  make install
  chmod a+rx contrib/git-jump/git-jump
  mkdir -p "${install_prefix}/share/doc/git"
  cp -Ruv contrib "${install_prefix}/share/doc/git/"
cd -

. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
  */home/*) . ${hooks_dir}/post_install.sh ;;
  *) . ${hooks_dir}/post_root_install.sh ;;
esac
<< EOF cat > "${XDG_CONFIG_HOME?}/environment.d/60-${package_name}.conf"
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_CREDENTIAL_STORE="\${XDG_RUNTIME_DIR}/git"
EOF
