TMPDIR := $(shell mktemp -d)

%/src/gawk-${VERSION} : ${TMPDIR}/gawk-${VERSION}.tar.lz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/gawk-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/gawk/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/gawk/$(@F).sig"
	-gpg --verify "$@.sig"
