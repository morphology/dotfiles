#! /usr/bin/make -rf
include _defaults.mk

@install : ~/build/${package_name}
	install -D -s "$</actionlint" ~/bin/
	install -D -C "$</docs/*" "${XDG_DATA_HOME}/${package_name}"
	install -D -C "$</man/*.1" "${XDG_DATA_HOME}/man/man1"

%/build/actionlint : ${DOWNLOAD_DIR}/actionlint_${VERSION}_linux_amd64.tar.gz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"

${DOWNLOAD_DIR}/actionlint_% :
	wget -P "$(@D)" \
		"https://github.com/rhysd/actionlint/releases/download/v${VERSION}/$(@F)"
