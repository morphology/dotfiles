#! /usr/bin/env bash

cd "$installer_dir"
  stow --no-folding -S "${package_name?Are you using 'install-package'?}"
  systemd-tmpfiles --user --create || echo "Skipping systemd-tmpfiles" >&2
  install -d $XDG_DATA_HOME/gvfs-metadata $XDG_DATA_HOME/tracker
  make -rf "${package_name}.mk" \
    $XDG_DATA_HOME/gvfs-metadata/CACHEDIR.TAG \
    $XDG_DATA_HOME/tracker/CACHEDIR.TAG
cd -
