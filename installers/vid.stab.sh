#! /bin/sh
anitya_project_id=15959
package_name=vid.stab
packaging_prefix=/usr/local/stow
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh
make -rf "ffmpeg.mk" "$source_dir"

mkdir -p ~/build/${package_name}
cd ~/build/vid.stab
  cmake ${source_dir} -DCMAKE_INSTALL_PREFIX:PATH=${install_prefix}
  make -j -l $(nproc)
  make install
cd -

. ${hooks_dir}/post_root_install.sh
$SUDO ldconfig
