#! /usr/bin/make -rf
include _defaults.mk

%/src/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${VERSION}.tar.bz2
	tar -C "$(@D)" -xjf "$<"

${DOWNLOAD_DIR}/busybox-%:
	wget -P "$(@D)" "https://busybox.net/downloads/$(@F)" \
	                "https://busybox.net/downloads/$(@F).sig" \
	                "https://busybox.net/downloads/$(@F).sha256"
	-gpg --verify "$@.sig" "$@.sha256"
	cd "$(@D)" ; sha256sum -c "$@.sha256"
