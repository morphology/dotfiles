#! /bin/sh
set -e

. _hooks/setup.sh
go install github.com/wader/fq@latest
install -s ~/go/bin/fq "${install_prefix}/bin"
