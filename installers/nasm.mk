#! /usr/bin/env make -rf
VERSION ?= 2.15.05
include _defaults.mk

%/src/nasm-${VERSION} : ${DOWNLOAD_DIR}/nasm-${VERSION}.tar.bz2
	tar -C "$(@D)" -xaf "$<"
	cd "$@" ; sh autogen.sh

${DOWNLOAD_DIR}/nasm-% :
	wget -P "$(@D)" \
		"https://www.nasm.us/pub/nasm/releasebuilds/${VERSION}/$(@F)"
