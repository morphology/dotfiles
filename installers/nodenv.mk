${HOME}/.nodenv :
	git clone --depth 1 git@github.com:nodenv/nodenv.git $@
	git clone --depth 1 git@github.com:nodenv/node-build.git $@/plugins/node-build
	mkdir -p "${XDG_CONFIG_HOME}/bash"
	$@/bin/nodenv init - > ${XDG_CONFIG_HOME}/bash/nodenv.bash
