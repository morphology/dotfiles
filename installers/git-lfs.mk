#! /usr/bin/make -rf
install_prefix ?= ${packaging_prefix}/${package_name}-${VERSION}
include _defaults.mk
arch ?= linux-amd64

@install : ~/build/${package_name}-${VERSION}
	install -Ds "$</git-lfs" "${install_prefix}/bin/git-lfs"
	install -d "${install_prefix}/share/"
	cp -Ruv "$</man" "${install_prefix}/share/"

%/build/${package_name}-${VERSION} : ${DOWNLOAD_DIR}/${package_name}-${arch}-v${VERSION}.tar.gz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/${package_name}-% :
	wget -P "$(@D)" \
		"https://github.com/git-lfs/${package_name}/releases/download/v${VERSION}/$(@F)"
