#! /usr/bin/make -rf
XDG_DATA_HOME ?= ${HOME}/.local/share
BASH_COMPLETION_USER_DIR ?= ${XDG_DATA_HOME}/bash-completion

@install : @module ${BASH_COMPLETION_USER_DIR}/completions/${package_name} ${XDG_DATA_HOME}/man/man1/${package_name}

@module :
	pip install --user tqdm

%/man/man1/tqdm.1 : | @module
	mkdir -p "$(@D)"
	tqdm --manpath "$(@D)"

%/completions/tqdm : | @module
	mkdir -p "$(@D)"
	tqdm --comppath "$(@D)"
	bash -n "$@"
