#! /usr/bin/make -rf

DOTNET_VERSION ?= 6.0.403
VERSION ?= 2.299.1

TMPDIR := $(shell mktemp -d)

%/actions-runner : ${TMPDIR}/actions-runner-linux-x64-${VERSION}.tar.gz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"
	chown -R github-runner:nogroup "$@"

%/.dotnet : ${TMPDIR}/dotnet-sdk-${DOTNET_VERSION}-linux-x64.tar.gz
	mkdir -p "$@"
	tar -C "$@" -xaf "$<"
	chown -R github-runner:nogroup "$@"

${TMPDIR}/actions-runner-%.tar.gz :
	curl -o "$@" -fL "https://github.com/actions/runner/releases/download/v${VERSION}/$(@F)"
	$(file > $(@D)/SHA256SUMS,147c14700c6cb997421b9a239c012197f11ea9854cd901ee88ead6fe73a72c74  actions-runner-linux-x64-2.299.1.tar.gz)
	cd "$(@D)" ; sha256sum -c SHA256SUMS

# See:
# https://github.com/dotnet/core/blob/main/release-notes/6.0/linux-packages.md
# https://dotnet.microsoft.com/en-us/download/dotnet
# https://learn.microsoft.com/en-us/dotnet/core/install/linux-scripted-manual#scripted-install

${TMPDIR}/dotnet-runtime-6.0.11-linux-x64.% :
	curl -fL -o "$@" "https://download.visualstudio.microsoft.com/download/pr/367108bb-8782-4f0b-839d-c98191b7729a/94185f91ef33890816a5846a374b74b7/$(@F)"

${TMPDIR}/dotnet-sdk-6.0.403-linux-x64.% :
	curl -fL -o "$@" "https://download.visualstudio.microsoft.com/download/pr/1d2007d3-da35-48ad-80cc-a39cbc726908/1f3555baa8b14c3327bb4eaa570d7d07/$(@F)"


${TMPDIR}/dotnet-runtime-7.0.0-linux-x64.% :
	curl -fL -o "$@" "https://download.visualstudio.microsoft.com/download/pr/d4b0a69b-29cd-40ef-9e95-a8d16f0ff346/5844add76ae3917af9efd59d95e3cbd4/$(@F)"


