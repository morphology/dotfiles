#! /usr/bin/make -rf
VERSION ?= 2.2.2
TMPDIR := $(shell mktemp -d)

install : Hack SpaceMono

Hack : ${TMPDIR}/Hack.zip
	unzip -d "${XDG_DATA_HOME}/fonts" "$<" \
		"$@ Regular Nerd Font Complete.ttf" \
		"$@ Bold Nerd Font Complete.ttf" \
		"$@ Bold Italic Nerd Font Complete.ttf" \
		"$@ Italic Nerd Font Complete.ttf"

SpaceMono : ${TMPDIR}/SpaceMono.zip
	unzip -d "${XDG_DATA_HOME}/fonts" "$<" \
		"Space Mono Nerd Font Complete.ttf" \
		"Space Mono Bold Nerd Font Complete.ttf" \
		"Space Mono Bold Italic Nerd Font Complete.ttf" \
		"Space Mono Italic Nerd Font Complete.ttf"


${TMPDIR}/%.zip :
	curl -fL -o "$@" "https://github.com/ryanoasis/${package_name}/releases/download/v${VERSION}/$(@F)"
