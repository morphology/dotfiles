#! /usr/bin/make -rf
package_name := ov
VERSION ?= 0.30.0

include _defaults.mk

repo_directory := ${HOME}/go/pkg/mod
module_directory := $(lastword $(wildcard ${repo_directory}/github.com/noborus/ov@v${VERSION}/))

@install : ${install_prefix}/bin/${package_name} ${BASH_COMPLETION_USER_DIR}/completions/${package_name} ${XDG_CONFIG_HOME}/${package_name}/config.yaml

@bin ${module_directory} :
	go install github.com/noborus/${package_name}@latest

%/bin/ov : @bin
	install -s ~/go/bin/ov "$@"

%/completions/${package_name} %/bash_completions.d/${package_name} : | @bin
	mkdir -p "$(@D)"
	ov --completion bash > "$@"
	bash -n "$@"

%/ov/config.yaml : | ${module_directory}
	mkdir -p "$(@D)"
	install -C "$|/ov-less.yaml" "$@"
