#! /bin/sh
package_name=tmux
anitya_project_id=4980
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name?}
. ${hooks_dir?}/pre_install.sh
mkdir -p ~/build/${package_name}
cd ~/build/${package_name}
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$XDG_STATE_HOME" \
    --runstatedir="${XDG_RUNTIME_DIR}/${package_name}" \
    --sysconfdir=/etc
  make -j -l $(nproc)
  make install
cd -

. "${hooks_dir}/install_ready.sh"
case "$packaging_prefix" in
  */home/*)
    . ${hooks_dir}/post_install.sh
    make -rf "${package_name}.mk" ${BASH_COMPLETION_USER_DIR}/completions/${package_name}
    ;;
  *)
    . ${hooks_dir}/post_root_install.sh
    case "$packaging_prefix" in
      */stow/*)
        command_path="$( realpath -e ${packaging_prefix}/../bin/${package_name} )"
        if command -v update-shells && ! fgrep -q "$command_path" /var/lib/shells.state; then
          << EOF $SUDO tee -a /var/lib/shells.state
$command_path
EOF
          $SUDO update-shells
        fi
        ;;
    esac
    $SUDO make -rf "${package_name}.mk" package_name="$package_name" \
      /usr/local/share/bash-completion/completions/${package_name}
    ;;
esac

