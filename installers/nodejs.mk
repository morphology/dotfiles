#! /usr/bin/make -rf
include _defaults.mk

%/src/node-v${VERSION} : ${DOWNLOAD_DIR}/node-v${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${DOWNLOAD_DIR}/node-% : ${DOWNLOAD_DIR}/SHASUMS256.txt
	wget -O "$@" \
		"https://nodejs.org/dist/v${VERSION}/$(@F)"
	cd "$(@D)" && sha256sum -c --ignore-missing "$(<F)"

# Note the unusual spellings:
${DOWNLOAD_DIR}/SHASUMS256.txt : $(realpath ./${package_name}.gpg)
	wget -P "${@D}" \
		"https://nodejs.org/dist/v${VERSION}/$(@F)" \
		"https://nodejs.org/dist/v${VERSION}/$(@F).sig"
	gpgv --keyring "$<" "$@.sig" "$@"

nodejs.gpg :
	gpg --no-default-keyring --keyring="$@" \
		--auto-key-locate nodefault,wkd \
		--locate-keys A6023530FC53461FEC91F99C04CD3F2FDE079578

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix 
