#! /usr/bin/make -rf
VERSION = 15.35.7
include _defaults.mk


@install : ${DOWNLOAD_DIR}/teamviewer_${VERSION}_amd64.deb
	# "Download $(@F) from 'https://download.teamviewer.com/download/linux'"
	${SUDO} apt install "$+"

