#! /usr/bin/make -rf
DATA_VERSION ?= 1.0
include _defaults.mk

%/src/${package_name} : | ${TMPDIR}/data.zip
	gh repo clone eagledot/hachi "$@" -- -b ${VERSION} --depth=1
	unzip -d "$@" "${TMPDIR}/data.zip"

${TMPDIR}/data.zip :
	wget -O "$@" --no-clobber \
		"https://github.com/eagledot/hachi/releases/download/v${DATA_VERSION}/data.zip"

