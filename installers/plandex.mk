#! /usr/bin/make -rf
package_name := plandex
include _defaults.mk
VERSION != curl -fL 'https://raw.githubusercontent.com/plandex-ai/${package_name}/main/app/cli/version.txt'


@install : ${DOWNLOAD_DIR}/${package_name}_${VERSION}_linux_${arch}.tar.gz
	tar -C '${HOME}/bin' -xaf '$<'

%/${package_name} :
	gh repo clone 'plandex-ai/$(@F)' "$@" -- --depth=1 -b 'server/v${VERSION}'

${DOWNLOAD_DIR}/plandex_% :
	gh release -R 'plandex-ai/${package_name}' download 'cli/v${VERSION}' -p '$(@F)' -O '$@'
