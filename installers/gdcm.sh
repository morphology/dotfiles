#! /bin/sh
package_name=gdcm
anitya_project_id=883
set -e

. _hooks/setup.sh
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "/usr/local/src/${package_name}" )
. ${hooks_dir}/pre_install.sh

mkdir -p ~/build/gdcm
cd ~/build/gdcm
	cmake -DCMAKE_INSTALL_PREFIX="$install_prefix" \
		-DCMAKE_BUILD_TYPE=Release \
		-DGDCM_BUILD_SHARED_LIBS=ON \
		-DGDCM_WRAP_PYTHON=ON \
		-DGDCM_WRAP_CSHARP=OFF \
		-DGDCM_WRAP_JAVA=OFF \
		-DGDCM_WRAP_PHP=OFF \
		-DGDCM_USE_VTK=OFF \
		-DGDCM_BUILD_APPLICATIONS=ON \
		-DGDCM_BUILD_TESTING=OFF \
		-DGDCM_DOCUMENTATION=OFF \
		-DDCM_BUILD_EXAMPLES=OFF \
		$source_dir/
	make -j -l $(nproc)
	make install
cd -

. ${hooks_dir}/install_ready.sh
case "$packaging_prefix" in
	*/stow/*)
		<< EOF cat > "${install_prefix}/.stow-local-ignore"
lib/pkgconfig/libopenjp2.pc
EOF
		;;
esac
case "$packaging_prefix" in
	*/home/*) . ${hooks_dir}/post_install.sh ;;
	*) . ${hooks_dir}/post_root_install.sh ;;
esac

# vim: ff=unix tabstop=3 shiftwidth=3 noexpandtab
