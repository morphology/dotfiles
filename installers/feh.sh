#! /bin/sh
export VERSION='3.8'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  export PREFIX="$install_prefix"
  make -j -l $(nproc) exif=1 inotify=1
  make install
cd -

cd "$installer_dir"
  stow -S --no-folding "${package_name}"
  . hooks/post_install.sh
  install -s -D "${install_prefix}/bin/feh" "${HOME}/bin"
cd -
