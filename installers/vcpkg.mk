#! /usr/bin/make -rf
VERSION ?= 2023.04.15

TMPDIR := $(shell mktemp -d)

%/build/vcpkg-${VERSION} :
	git clone --depth=1 -b ${VERSION} "https://github.com/microsoft/vcpkg" "$@"
	$@/bootstrap-vcpkg.sh -disableMetrics
