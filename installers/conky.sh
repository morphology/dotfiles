#! /bin/sh
export VERSION='1.12.2'
set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}" )

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

mkdir -p build/conky
cd build/conky
  cmake "$source_dir" -DCMAKE_INSTALL_PREFIX="$install_prefix"
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
  stow -S --no-folding "$package_name"
  make -rf "${package_name}.mk" $XDG_CONFIG_HOME/conky/conky.conf
cd -
