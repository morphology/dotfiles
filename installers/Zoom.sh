#! /bin/sh
package_name=Zoom
case $( uname -m ) in
  i686)
    arch=i386
    export VERSION=5.4.53391.1108
    ;;
  x86_64)
    arch=amd64
    ;;
esac
export arch
set -e
make -rf "${package_name}.mk"
