TMPDIR := $(shell mktemp -d)

%/src/${package_name}-${VERSION} : ${TMPDIR}/${package_name}-${VERSION}.tar.xz
	tar -C "$(@D)" -xaf "$<"

${TMPDIR}/grep-% :
	curl -fL -o "$@"     "http://ftpmirror.gnu.org/gnu/grep/$(@F)" \
	         -o "$@.sig" "http://ftpmirror.gnu.org/gnu/grep/$(@F).sig"
	-gpg --verify "$@.sig"
