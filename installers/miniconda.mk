#! /usr/bin/make -rf
include ${installer_dir}/Makefile

install : ${TMPDIR}/Miniconda3-${VERSION}-Linux-x86_64.sh ${install_prefix}/pkgs/CACHEDIR.TAG
	sh "$<" -p "${install_prefix}" -u

${TMPDIR}/Miniconda3-%.sh :
	curl -fL -o "$@" "https://repo.anaconda.com/${package_name}/$(@F)"
