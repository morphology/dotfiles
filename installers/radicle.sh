#! /bin/sh
set -eu
src_dir="$1"

# radicle-httpd is optional for laptops
cd "$src_dir"
	for subdir in radicle-cli radicle-node radicle-remote-helper; do
		cargo install --force --locked \
			--path "$subdir"
	done
cd -
