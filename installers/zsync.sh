#! /bin/sh
project_id=13677
check_url="https://release-monitoring.org/api/v2/versions/?project_id=${project_id}"
export VERSION=$( curl -fL "$check_url" | jq -r '.latest_version' )

set -eu
install_prefix="${packaging_prefix}/${package_name}-${VERSION}"
source_dir=$( realpath "src/${package_name}-${VERSION}" )

case "$install_prefix" in
  */home/*)
    localstatedir=$XDG_STATE_HOME
    sysconfdir="${HOME}/etc"
    ;;
  *)
    localstatedir=/var
    sysconfdir=/etc
    ;;
esac

cd "$installer_dir"
  . hooks/pre_install.sh
cd -

cd "$source_dir"
  "$source_dir/configure" \
    --prefix="$install_prefix" \
    --localstatedir="$localstatedir" \
    --sysconfdir="$sysconfdir" \
    --with-ssl-dir="${OPENSSL_ROOT_DIR?}"
  make all -j -l $(nproc)
  make install
cd -

cd "$installer_dir"
  . hooks/post_install.sh
cd -
