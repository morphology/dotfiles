#! /bin/sh -eu
VERSION=1.16
package_name=seergdb
set -e

. _hooks/setup.sh
source_dir=~/src/${package_name}
. ${hooks_dir}/pre_install.sh
make -rf "${package_name}.mk" "$source_dir"
mkdir -p ~/build/${package_name}
cd ~/build/${package_name}
  cmake ${source_dir}/src
  make
  install -s seergdb ${install_prefix}/bin/seergdb
cd -

