# Install scripts

1. Each installation produces a JSON based on release-monitoring.org. Usually the latest release is chosen.
2. Installation to either /usr/local/stow, ~/.local/stow, or a place preferred by the package (i.e. /opt)
3. Upgrading is not as easy. Most packages will pause for you to un-stow the old installation. This always has side-
   effects.

See also
--------

This replaces https://gitlab.com/morphology/stowport

