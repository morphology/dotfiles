#! /bin/sh

# During installation
mkdir -p var/cache var/lib var/tmp
chattr +c com home opt usr
chattr +C tmp var/cache var/tmp

### btrfs:
# add compress=zstd for whole filesystems
# add discard=async for SSD
# autodefrag

for subdir in \
	opt \
	swap \
	srv \
	var/backups \
	var/log \
	var/lib/docker \
	var/lib/lxd \
	var/lib/machines \
	var/lib/portables \
	var/opt; do
	btrfs subvolume create "$subdir"
done
btrfs filesystem mkswapfile --size 32g --uuid clear /swap/swapfile
# Create a swap entry
cat >> /etc/fstab << EOF
/swap/swapfile none swap defaults 0 0
EOF

# After installation
chown -R :staff /usr/local /usr/src
chmod -R g+rwXs /usr/local /usr/src

# To add a LUKS keyscript:
# 1. Modify /etc/crypttab with a keyscript argument
# 2. Create the keyscript to echo password
# 3. `keyscript > /dev/shm/key`
# 4. `cryptsetup luksAddKey $dev /dev/shm/key`
# 5. `update-initramfs -u` should pick it up
